ForestGOMP
----------

ForestGOMP is an OpenMP 2.5 run-time support library compatible with GNU
OpenMP's libgomp.  It builds on the Marcel user-level NxM threading
library (http://runtime.bordeaux.inria.fr/marcel/).

Marcel offers appealing features for high-performance computing, such as
topology-aware scheduling over hierarchical architectures ("bubble
scheduling").  The run-forest(1) tool's `--scheduler' option offers a
simple way to experiment with this feature.


Requirements:
=============

* Marcel 2.90 (part of the PM2 software framework)
  http://gforge.inria.fr/frs/download.php/22408/marcel-2.90.tar.gz

  Newer versions can be obtained from:
  svn checkout svn://scm.gforge.inria.fr/svn/pm2/trunk

  See http://runtime.bordeaux.inria.fr/marcel/ for details.

  This release of Marcel contains "flavors" (i.e., compilation option
  sets) suitable for ForestGOMP.  The ForestGOMP-dedicated PM2 flavors
  are:

    - `libpthread', which includes a GNU NPTL-compatible libpthread.so
      shared library, on GNU systems (more on that below);

    - `marcel-forestgomp-opt', optimized version of Marcel and
      Marcel/BubbleSched;
    
    - `marcel-forestgomp-debug', debugging version.
     
  Advanced users can use the `marcel-forestgomp-fxtdebug' flavor to
  generate Flash animations showing the scheduler behaviour (like
  thread distribution, bubble explosions, work stealing behaviour,
  ...) during the execution of your application.  To use this flavor,
  you need the FxT library, which you can get with:
  
    cvs -d :pserver:anonymous@cvs.sv.gnu.org:/sources/fkt co FxT

  You can also create your own PM2 flavor, but you have to activate
  these options in the Marcel module:

    - pmarcel, enable_keys, enable_stats, marcel-numa, standard_main

* Marcel-unaware programs such as OpenMP applications running on top of
  ForestGOMP need to be protected: they make direct libc calls, some of
  which use functions of the threading library, which is not aware of
  Marcel's user-level threads.  This can lead to crashes, or, most
  likely, deadlocks.

  To work around this, the PukABI ABI compatibility library must be
  preloaded (PukABI is part of PadicoTM, available at
  http://gforge.inria.fr/projects/padico).  Preloading makes sure the
  application gets to see PukABI's "Marcel-safe" version of some of
  libc's functions.

  The `libpthread' PM2 flavor, which is available on GNU systems such
  as GNU/Linux, builds a libpthread.so library that is
  binary-compatible with GNU's libpthread and uses PukABI.

  ForestGOMP's `configure' script will automatically detect it if it's
  available.  It's an optional, but highly recommended, dependency.

  ForestGOMP applications can then be run with:

    run-forest my-application

* If you don't want bubbles, you may pass --disable-marcel-bubbles
  - NUMA won't be need in the flavor anymore


How to configure forestgomp:
===========================

* Go to libgomp/
* autoreconf -i --force (to generate "configure" related files)
  This is only needed when building from the repository.
* Go to build/
* ../configure --prefix=/INSTALLATION/PATH
* make && make install
* The dynamic and static libs will be built in libgomp/build/.libs/
  and installed to /INSTALLATION/PATH/lib .

Notes:
======
* By default, libgomp builds both 32 and 64bits libraries. Pass
  --disable-multilib to only build for your machine
  (required for 32bits processors, at least)

If you modify the configure.ac scripts:
=======================================
* Go to libgomp/
* Run "autoreconf -i --force" (you don't even need it if configured with
  `--enable-maintainer-mode').
* This will update configure and the .in files.
  + These files will be committed next time you commit
    (so that people don't need to run autoconf and autoheader again)

How the repository is organized:
================================
* trunk/ contains the latest forestgomp tree
* branches/gcc-4.2-*/ contains raw gcc-4.2 snapshots,
  with only the required files to make libgomp build fine
* branches/forestgomp-* contains forestgomp modified gcc-4.2 snapshots

To update the tree to a new gcc-4.2 release:
* Save the current forestgomp tree by creating a forestgomp branch copied from the trunk
  + svn cp $(svnpath)/trunk/ $(svnpath)/branches/forestgomp-<date>-gcc-4.2-<version>/
* Copy old gcc-4.2 snapshot into a new branch
  + svn cp $(svnpath)/branches/gcc-4.2-<oldversion>/ $(svnpath)/branches/gcc-4.2-<newversion>/
* Put the new raw gcc-4.2 in the new branch 
  + Copy all gcc-4.2 files in the branch
  + Run svn st and remove all new files
  + Check that autoconf, autoheader, ./configure and make still work
    - If not, find the missing files, and svn add them
  + Commit the changes
* Merge difference between the old gcc-4.2 branch and the new one into the trunk
  + svn merge $(svnpath)/branches/gcc-4.2-<oldversion>/ $(svnpath)/branches/gcc-4.2-<newversion>/ trunk/
* Commit the changes in the trunk
