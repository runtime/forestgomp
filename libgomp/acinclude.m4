dnl -*- Autoconf -*-

dnl ----------------------------------------------------------------------
dnl This whole bit snagged from libgfortran.

dnl Check whether the target supports __sync_*_compare_and_swap.
AC_DEFUN([LIBGOMP_CHECK_SYNC_BUILTINS], [
  AC_CACHE_CHECK([whether the target supports __sync_*_compare_and_swap],
		 [ac_cv_have_sync_builtins], [
  AC_LINK_IFELSE([AC_LANG_PROGRAM([int foo, bar;],
                   [bar = __sync_val_compare_and_swap(&foo, 0, 1);])],
                   [ac_cv_have_sync_builtins=yes],
                   [ac_cv_have_sync_builtins=no])])
  if test "x$ac_cv_have_sync_builtins" = "xyes"; then
    AC_DEFINE(HAVE_SYNC_BUILTINS, 1,
	      [Define to 1 if the target supports __sync_*_compare_and_swap])
  fi])

dnl Check whether the target supports hidden visibility.
AC_DEFUN([LIBGOMP_CHECK_ATTRIBUTE_VISIBILITY], [
  AC_CACHE_CHECK([whether the target supports hidden visibility],
		 ac_cv_have_attribute_visibility, [
  save_CFLAGS="$CFLAGS"
  CFLAGS="$CFLAGS -Werror"
  AC_TRY_COMPILE([void __attribute__((visibility("hidden"))) foo(void) { }],
		 [], ac_cv_have_attribute_visibility=yes,
		 ac_cv_have_attribute_visibility=no)
  CFLAGS="$save_CFLAGS"])
  if test $ac_cv_have_attribute_visibility = yes; then
    AC_DEFINE(HAVE_ATTRIBUTE_VISIBILITY, 1,
      [Define to 1 if the target supports __attribute__((visibility(...))).])
  fi])

dnl Check whether the target supports dllexport
AC_DEFUN([LIBGOMP_CHECK_ATTRIBUTE_DLLEXPORT], [
  AC_CACHE_CHECK([whether the target supports dllexport],
		 ac_cv_have_attribute_dllexport, [
  save_CFLAGS="$CFLAGS"
  CFLAGS="$CFLAGS -Werror"
  AC_TRY_COMPILE([void __attribute__((dllexport)) foo(void) { }],
		 [], ac_cv_have_attribute_dllexport=yes,
		 ac_cv_have_attribute_dllexport=no)
  CFLAGS="$save_CFLAGS"])
  if test $ac_cv_have_attribute_dllexport = yes; then
    AC_DEFINE(HAVE_ATTRIBUTE_DLLEXPORT, 1,
      [Define to 1 if the target supports __attribute__((dllexport)).])
  fi])

dnl Check whether the target supports symbol aliases.
AC_DEFUN([LIBGOMP_CHECK_ATTRIBUTE_ALIAS], [
  AC_CACHE_CHECK([whether the target supports symbol aliases],
		 ac_cv_have_attribute_alias, [
  AC_TRY_LINK([
void foo(void) { }
extern void bar(void) __attribute__((alias("foo")));],
    [bar();], ac_cv_have_attribute_alias=yes, ac_cv_have_attribute_alias=no)])
  if test $ac_cv_have_attribute_alias = yes; then
    AC_DEFINE(HAVE_ATTRIBUTE_ALIAS, 1,
      [Define to 1 if the target supports __attribute__((alias(...))).])
  fi])

])

dnl ----------------------------------------------------------------------
dnl This whole bit snagged from libstdc++-v3.

dnl
dnl LIBGOMP_ENABLE
dnl    (FEATURE, DEFAULT, HELP-ARG, HELP-STRING)
dnl    (FEATURE, DEFAULT, HELP-ARG, HELP-STRING, permit a|b|c)
dnl    (FEATURE, DEFAULT, HELP-ARG, HELP-STRING, SHELL-CODE-HANDLER)
dnl
dnl See docs/html/17_intro/configury.html#enable for documentation.
dnl
m4_define([LIBGOMP_ENABLE],[dnl
m4_define([_g_switch],[--enable-$1])dnl
m4_define([_g_help],[AC_HELP_STRING(_g_switch$3,[$4 @<:@default=$2@:>@])])dnl
 AC_ARG_ENABLE($1,_g_help,
  m4_bmatch([$5],
   [^permit ],
     [[
      case "$enableval" in
       m4_bpatsubst([$5],[permit ])) ;;
       *) AC_MSG_ERROR(Unknown argument to enable/disable $1) ;;
          dnl Idea for future:  generate a URL pointing to
          dnl "onlinedocs/configopts.html#whatever"
      esac
     ]],
   [^$],
     [[
      case "$enableval" in
       yes|no) ;;
       *) AC_MSG_ERROR(Argument to enable/disable $1 must be yes or no) ;;
      esac
     ]],
   [[$5]]),
  [enable_]m4_bpatsubst([$1],-,_)[=][$2])
m4_undefine([_g_switch])dnl
m4_undefine([_g_help])dnl
])


dnl
dnl If GNU ld is in use, check to see if tricky linker opts can be used.  If
dnl the native linker is in use, all variables will be defined to something
dnl safe (like an empty string).
dnl
dnl Defines:
dnl  SECTION_LDFLAGS='-Wl,--gc-sections' if possible
dnl  OPT_LDFLAGS='-Wl,-O1' if possible
dnl  LD (as a side effect of testing)
dnl Sets:
dnl  with_gnu_ld
dnl  libgomp_gnu_ld_version (possibly)
dnl
dnl The last will be a single integer, e.g., version 1.23.45.0.67.89 will
dnl set libgomp_gnu_ld_version to 12345.  Zeros cause problems.
dnl
AC_DEFUN([LIBGOMP_CHECK_LINKER_FEATURES], [
  # If we're not using GNU ld, then there's no point in even trying these
  # tests.  Check for that first.  We should have already tested for gld
  # by now (in libtool), but require it now just to be safe...
  test -z "$SECTION_LDFLAGS" && SECTION_LDFLAGS=''
  test -z "$OPT_LDFLAGS" && OPT_LDFLAGS=''
  AC_REQUIRE([AC_PROG_LD])
  AC_REQUIRE([AC_PROG_AWK])

  # The name set by libtool depends on the version of libtool.  Shame on us
  # for depending on an impl detail, but c'est la vie.  Older versions used
  # ac_cv_prog_gnu_ld, but now it's lt_cv_prog_gnu_ld, and is copied back on
  # top of with_gnu_ld (which is also set by --with-gnu-ld, so that actually
  # makes sense).  We'll test with_gnu_ld everywhere else, so if that isn't
  # set (hence we're using an older libtool), then set it.
  if test x${with_gnu_ld+set} != xset; then
    if test x${ac_cv_prog_gnu_ld+set} != xset; then
      # We got through "ac_require(ac_prog_ld)" and still not set?  Huh?
      with_gnu_ld=no
    else
      with_gnu_ld=$ac_cv_prog_gnu_ld
    fi
  fi

  # Start by getting the version number.  I think the libtool test already
  # does some of this, but throws away the result.
  changequote(,)
  ldver=`$LD --version 2>/dev/null | head -1 | \
         sed -e 's/GNU ld \(version \)\{0,1\}\(([^)]*) \)\{0,1\}\([0-9.][0-9.]*\).*/\3/'`
  changequote([,])
  libgomp_gnu_ld_version=`echo $ldver | \
         $AWK -F. '{ if (NF<3) [$]3=0; print ([$]1*100+[$]2)*100+[$]3 }'`

  # Set --gc-sections.
  if test "$with_gnu_ld" = "notbroken"; then
    # GNU ld it is!  Joy and bunny rabbits!

    # All these tests are for C++; save the language and the compiler flags.
    # Need to do this so that g++ won't try to link in libstdc++
    ac_test_CFLAGS="${CFLAGS+set}"
    ac_save_CFLAGS="$CFLAGS"
    CFLAGS='-x c++  -Wl,--gc-sections'

    # Check for -Wl,--gc-sections
    # XXX This test is broken at the moment, as symbols required for linking
    # are now in libsupc++ (not built yet).  In addition, this test has
    # cored on solaris in the past.  In addition, --gc-sections doesn't
    # really work at the moment (keeps on discarding used sections, first
    # .eh_frame and now some of the glibc sections for iconv).
    # Bzzzzt.  Thanks for playing, maybe next time.
    AC_MSG_CHECKING([for ld that supports -Wl,--gc-sections])
    AC_TRY_RUN([
     int main(void)
     {
       try { throw 1; }
       catch (...) { };
       return 0;
     }
    ], [ac_sectionLDflags=yes],[ac_sectionLDflags=no], [ac_sectionLDflags=yes])
    if test "$ac_test_CFLAGS" = set; then
      CFLAGS="$ac_save_CFLAGS"
    else
      # this is the suspicious part
      CFLAGS=''
    fi
    if test "$ac_sectionLDflags" = "yes"; then
      SECTION_LDFLAGS="-Wl,--gc-sections $SECTION_LDFLAGS"
    fi
    AC_MSG_RESULT($ac_sectionLDflags)
  fi

  # Set linker optimization flags.
  if test x"$with_gnu_ld" = x"yes"; then
    OPT_LDFLAGS="-Wl,-O1 $OPT_LDFLAGS"
  fi

  AC_SUBST(SECTION_LDFLAGS)
  AC_SUBST(OPT_LDFLAGS)
])


dnl
dnl Add version tags to symbols in shared library (or not), additionally
dnl marking other symbols as private/local (or not).
dnl
dnl --enable-symvers=style adds a version script to the linker call when
dnl       creating the shared library.  The choice of version script is
dnl       controlled by 'style'.
dnl --disable-symvers does not.
dnl  +  Usage:  LIBGOMP_ENABLE_SYMVERS[(DEFAULT)]
dnl       Where DEFAULT is either 'yes' or 'no'.  Passing `yes' tries to
dnl       choose a default style based on linker characteristics.  Passing
dnl       'no' disables versioning.
dnl
AC_DEFUN([LIBGOMP_ENABLE_SYMVERS], [

LIBGOMP_ENABLE(symvers,yes,[=STYLE],
  [enables symbol versioning of the shared library],
  [permit yes|no|gnu])

# If we never went through the LIBGOMP_CHECK_LINKER_FEATURES macro, then we
# don't know enough about $LD to do tricks...
AC_REQUIRE([LIBGOMP_CHECK_LINKER_FEATURES])
# FIXME  The following test is too strict, in theory.
if test $enable_shared = no ||
        test "x$LD" = x ||
        test x$libgomp_gnu_ld_version = x; then
  enable_symvers=no
fi

# Check to see if libgcc_s exists, indicating that shared libgcc is possible.
if test $enable_symvers != no; then
  AC_MSG_CHECKING([for shared libgcc])
  ac_save_CFLAGS="$CFLAGS"
  CFLAGS=' -lgcc_s'
  AC_TRY_LINK(, [return 0;], libgomp_shared_libgcc=yes, libgomp_shared_libgcc=no)
  CFLAGS="$ac_save_CFLAGS"
  if test $libgomp_shared_libgcc = no; then
    cat > conftest.c <<EOF
int main (void) { return 0; }
EOF
changequote(,)dnl
    libgomp_libgcc_s_suffix=`${CC-cc} $CFLAGS $CPPFLAGS $LDFLAGS \
			     -shared -shared-libgcc -o conftest.so \
			     conftest.c -v 2>&1 >/dev/null \
			     | sed -n 's/^.* -lgcc_s\([^ ]*\) .*$/\1/p'`
changequote([,])dnl
    rm -f conftest.c conftest.so
    if test x${libgomp_libgcc_s_suffix+set} = xset; then
      CFLAGS=" -lgcc_s$libgomp_libgcc_s_suffix"
      AC_TRY_LINK(, [return 0;], libgomp_shared_libgcc=yes)
      CFLAGS="$ac_save_CFLAGS"
    fi
  fi
  AC_MSG_RESULT($libgomp_shared_libgcc)
fi

# For GNU ld, we need at least this version.  The format is described in
# LIBGOMP_CHECK_LINKER_FEATURES above.
libgomp_min_gnu_ld_version=21400
# XXXXXXXXXXX libgomp_gnu_ld_version=21390

# Check to see if unspecified "yes" value can win, given results above.
# Change "yes" into either "no" or a style name.
if test $enable_symvers = yes; then
  if test $with_gnu_ld = yes &&
     test $libgomp_shared_libgcc = yes;
  then
    if test $libgomp_gnu_ld_version -ge $libgomp_min_gnu_ld_version ; then
      enable_symvers=gnu
    else
      # The right tools, the right setup, but too old.  Fallbacks?
      AC_MSG_WARN(=== Linker version $libgomp_gnu_ld_version is too old for)
      AC_MSG_WARN(=== full symbol versioning support in this release of GCC.)
      AC_MSG_WARN(=== You would need to upgrade your binutils to version)
      AC_MSG_WARN(=== $libgomp_min_gnu_ld_version or later and rebuild GCC.)
      if test $libgomp_gnu_ld_version -ge 21200 ; then
        # Globbing fix is present, proper block support is not.
        dnl AC_MSG_WARN([=== Dude, you are soooo close.  Maybe we can fake it.])
        dnl enable_symvers=???
        AC_MSG_WARN([=== Symbol versioning will be disabled.])
        enable_symvers=no
      else
        # 2.11 or older.
        AC_MSG_WARN([=== Symbol versioning will be disabled.])
        enable_symvers=no
      fi
    fi
  else
    # just fail for now
    AC_MSG_WARN([=== You have requested some kind of symbol versioning, but])
    AC_MSG_WARN([=== either you are not using a supported linker, or you are])
    AC_MSG_WARN([=== not building a shared libgcc_s (which is required).])
    AC_MSG_WARN([=== Symbol versioning will be disabled.])
    enable_symvers=no
  fi
fi

AM_CONDITIONAL(LIBGOMP_BUILD_VERSIONED_SHLIB, test $enable_symvers != no)
AC_MSG_NOTICE(versioning on shared library symbols is $enable_symvers)
])



dnl PM2_CHECK_AVAILABILITY
dnl
dnl Check that all things PM2/Marcel are available and usable.  Define
dnl $PM2_FLAVOR if not already defined, substitute `PM2_CFLAGS',
dnl `PM2_LDFLAGS' and similar variables.
AC_DEFUN([PM2_CHECK_AVAILABILITY], [
  AC_ARG_WITH([pm2],
    [AS_HELP_STRING([--with-pm2=DIRECTORY],
      [use DIRECTORY as the PM2/Marcel root])],
    [dnl The `--with-pm2' option was passed, so use that.
     PM2_ROOT="$withval"
     PM2_CONFIG="$PM2_ROOT/bin/pm2-config"
     if ! test -x "$PM2_CONFIG"; then
       AC_MSG_ERROR([`$PM2_ROOT/bin/pm2-config' is not executable.])
     fi],
    [dnl The `--with-pm2' option wasn't specified, so try to guess.
     if test "x$PM2_ROOT" != "x"; then
       dnl Honor the user's $PM2_ROOT.
       PM2_CONFIG="$PM2_ROOT/bin/pm2-config"
       if ! test -x "$PM2_CONFIG"; then
	 AC_MSG_ERROR([`$PM2_ROOT/bin/pm2-config' is not executable.])
       fi
     else
       dnl Look for `pm2-config', determine PM2_ROOT from that.
       AC_PATH_PROG([PM2_CONFIG], [pm2-config], [not-found])
       if test "x$PM2_CONFIG" = "xnot-found"; then
         AC_MSG_ERROR([The `pm2-config' program from PM2 was not found.  Use `--with-pm2'.])
       fi

       PM2_ROOT="`dirname $PM2_CONFIG`"
       PM2_ROOT="`dirname $PM2_ROOT`"
       if ! test -d "$PM2_ROOT"; then
         AC_MSG_ERROR([Failed to determine the PM2 root directory.  Use `--with-pm2'.])
       fi
     fi])

  AC_MSG_CHECKING([for PM2/Marcel source tree root])
  AC_MSG_RESULT([$PM2_ROOT])

  AC_SUBST([PM2_ROOT])
  AC_SUBST([PM2_CONFIG])

  dnl Change the path so that future `AC_PATH_PROG' calls for PM2 work.
  PATH="$PM2_ROOT/bin:$PATH"
  export PATH

  AC_ARG_WITH([pm2-flavor],
    AS_HELP_STRING([--with-pm2-flavor=FLAVOR],
      [Use FLAVOR as the PM2/Marcel flavor]),
    [PM2_FLAVOR="$withval"])


  if test "x$PM2_FLAVOR" = "x"; then
    PM2_FLAVOR="`$PM2_CONFIG --getvar=PM2_FLAVOR`"
    if test "x$PM2_FLAVOR" = "x"; then
       AC_MSG_WARN([Cannot determine the current default PM2 flavor; falling back to `default'.])
       PM2_FLAVOR="default"
    fi

    # Draw the user's attention since they might want another flavor.
    AC_MSG_NOTICE([The `--with-pm2-flavor' option wasn't used, using the `$PM2_FLAVOR' flavor.])
  fi

  AC_MSG_CHECKING([for PM2 flavor `$PM2_FLAVOR'])
  if "$PM2_CONFIG" --flavor="$PM2_FLAVOR" 2> /dev/null >/dev/null; then
    AC_MSG_RESULT([ok])
  else
    AC_MSG_RESULT([flavor does not exist])
    AC_MSG_ERROR([PM2 flavor \`$PM2_FLAVOR' does not exist.])
  fi

  PM2_LIBDIR="`$PM2_CONFIG --flavor=$PM2_FLAVOR --ld-library-path`"

  AC_SUBST([PM2_FLAVOR])
  AC_SUBST([PM2_LIBDIR])

  PM2_CFLAGS="`$PM2_CONFIG --flavor=$PM2_FLAVOR --cflags`"
  PM2_LDFLAGS="`$PM2_CONFIG --flavor=$PM2_FLAVOR --libs`"
  AC_SUBST(PM2_LDFLAGS)
  AC_SUBST(PM2_CFLAGS)
])

dnl PM2_CHECK BODY
dnl
dnl Run BODY with PM2's CFLAGS and LDFLAGS correctly set.  CFLAGS and
dnl LDFLAGS are left unchanged.
AC_DEFUN([PM2_CHECK], [
  AC_REQUIRE([PM2_CHECK_AVAILABILITY])

  ac_pm2_save_CFLAGS="$CFLAGS"
  ac_pm2_save_LDFLAGS="$LDFLAGS"
  ac_pm2_save_LD_LIBRARY_PATH="$LD_LIBRARY_PATH"

  CFLAGS="$PM2_CFLAGS $CFLAGS"
  LDFLAGS="$PM2_LDFLAGS $LDFLAGS"

  if test -n "$LD_LIBRARY_PATH"; then
    LD_LIBRARY_PATH="`$PM2_CONFIG --flavor=$PM2_FLAVOR --libdir marcel`:$LD_LIBRARY_PATH"
  else
    LD_LIBRARY_PATH="`$PM2_CONFIG --flavor=$PM2_FLAVOR --libdir marcel`"
  fi
  export LD_LIBRARY_PATH

  $1

  LD_LIBRARY_PATH="$ac_pm2_save_LD_LIBRARY_PATH"
  LDFLAGS="$ac_pm2_save_LDFLAGS"
  CFLAGS="$ac_pm2_save_CFLAGS"

  export LD_LIBRARY_PATH
])

dnl MARCEL_CHECK_HEADER
dnl
dnl Make sure <marcel.h> is usable.
AC_DEFUN([MARCEL_CHECK_HEADER], [
  PM2_CHECK([
    AC_CHECK_HEADER([marcel.h], [],
      [AC_MSG_ERROR([Marcel's headers are either not available or broken.  See `config.log'.])])
  ])
])

dnl MARCEL_CHECK_FEATURE FEATURE [ACTION-IF-DEFINED [ACTION-IF-NOT-DEFINED]]
dnl
dnl Check whether (Marcel) macro FEATURE is defined.
AC_DEFUN([MARCEL_CHECK_FEATURE], [
  AC_REQUIRE([MARCEL_CHECK_HEADER])
  AC_MSG_CHECKING([whether Marcel macro `]$1[' is defined])

  PM2_CHECK([
    AC_COMPILE_IFELSE([
      #include <marcel.h>
      #ifndef ]$1[
      #error "Marcel macro undefined"
      #endif],
      [ac_marcel_macro_defined=yes], [ac_marcel_macro_defined=no])])
  AC_MSG_RESULT([$ac_marcel_macro_defined])

  if test "x$ac_marcel_macro_defined" = "xyes"; then
    :
    $2
  else
    :
    $3
  fi])])

dnl MARCEL_REQUIRE_FEATURES FEATURE...
dnl
dnl Check that all (Marcel) macros listed are defined.
dnl Abort with an error message if one of the macros isn't found.
AC_DEFUN([MARCEL_REQUIRE_FEATURES], [
  m4_foreach_w([MARCEL_MACRO], [$1], [
    MARCEL_CHECK_FEATURE(m4_defn([MARCEL_MACRO]),
      [:],
      [AC_MSG_ERROR([Required Marcel macro `]m4_defn([MARCEL_MACRO])[' not defined, check your PM2 flavor.])])
  ])
])

dnl MARCEL_CHECK_LIBPTHREAD VAR
dnl
dnl Check whether Marcel's ABI-compatible `libpthread.so' and the
dnl `pm2-libpthread' program are available.  Set VAR to `yes' or `no'
dnl depending on whether it's available and usable.  Substitute
dnl `HAVE_MARCEL_LIBPTHREAD', which is set to "yes" or "no".
AC_DEFUN([MARCEL_CHECK_LIBPTHREAD], [
  MARCEL_CHECK_FEATURE([MARCEL_LIBPTHREAD],
    [HAVE_MARCEL_LIBPTHREAD="yes"],
    [HAVE_MARCEL_LIBPTHREAD="no"
     AC_MSG_WARN([Marcel's ABI-compatible libpthread not available but highly recommended.])
     AC_MSG_WARN([PM2 flavor `libpthread' provides it.])])

  AM_CONDITIONAL([HAVE_MARCEL_LIBPTHREAD],
    [test "x$HAVE_MARCEL_LIBPTHREAD" = "xyes"])

  AC_SUBST([HAVE_MARCEL_LIBPTHREAD])
  $1="$HAVE_MARCEL_LIBPTHREAD"

  # A comment for use in shell scripts.
  if test "x$HAVE_MARCEL_LIBPTHREAD" = "xyes"; then
     HAVE_MARCEL_LIBPTHREAD_COMMENT=""
  else
     HAVE_MARCEL_LIBPTHREAD_COMMENT="#"
  fi
  AC_SUBST([HAVE_MARCEL_LIBPTHREAD_COMMENT])

  AC_PATH_PROG([PM2_LIBPTHREAD], [pm2-libpthread],
    [AC_MSG_ERROR([Marcel's `pm2-libpthread' program could not be found.])],
    [$PATH:$PM2_ROOT/bin])
])

dnl MARCEL_CHECK_TLS
dnl
dnl Check whether the compiler and Marcel support thread-local storage
dnl (TLS).  Based on `GCC_CHECK_TLS'.
AC_DEFUN([MARCEL_CHECK_TLS], [
  GCC_ENABLE(tls, yes, [], [Use thread-local storage])
  AC_CACHE_CHECK([whether the target supports thread-local storage],
		 ac_cv_have_tls, [
    AC_RUN_IFELSE([__thread int a; int b; int main() { return a = b; }],
      [dnl If the test case passed with dynamic linking, try again with
       dnl static linking, but only if static linking is supported (not
       dnl on Solaris 10).  This fails with some older Red Hat releases.
      save_LDFLAGS="$LDFLAGS"
      LDFLAGS="-static $LDFLAGS"
      AC_LINK_IFELSE([int main() { return 0; }],
	AC_RUN_IFELSE([__thread int a; int b; int main() { return a = b; }],
		      [ac_cv_have_tls=yes], [ac_cv_have_tls=no],[]),
	[ac_cv_have_tls=yes])
      LDFLAGS="$save_LDFLAGS"],
      [ac_cv_have_tls=no],
      [AC_COMPILE_IFELSE([__thread int foo;], [ac_cv_have_tls=yes], [ac_cv_have_tls=no])]
    )])

  MARCEL_CHECK_FEATURE([MA__PROVIDE_TLS],
    [have_marcel_tls="yes"], [have_marcel_tls="no"])
  if test "$enable_tls $ac_cv_have_tls $have_marcel_tls" = "yes yes yes"; then
    AC_DEFINE([HAVE_TLS], 1,
	      [Define to 1 if the target and Marcel support thread-local storage.])
  fi])

dnl PM2_CHECK_MAMI VAR
dnl
dnl Check whether MaMI (Marcel's NUMA memory management interface) is
dnl available.  Accordingly set VAR to "yes" or "no".
AC_DEFUN([PM2_CHECK_MAMI], [
  PM2_CHECK([
    AC_CHECK_LIB([mami], [mami_init],
      [HAVE_MARCEL_MAMI=yes], [HAVE_MARCEL_MAMI=no])

    if test "x$HAVE_MARCEL_MAMI" = "xyes"; then
      AC_CHECK_DECLS([mami_init, mami_task_attach], [],
	[HAVE_MARCEL_MAMI=no], [#include <mm_mami.h>])
    fi
  ])

  if test "x$HAVE_MARCEL_MAMI" = "xyes"; then
    AC_DEFINE([HAVE_MARCEL_MAMI], [1],
      [Define if Marcel's NUMA support (aka. MAMI) is available])
    FORESTGOMP_NUMA_SUPPORT=1
  else
    FORESTGOMP_NUMA_SUPPORT=0
  fi
  AC_SUBST([HAVE_MARCEL_MAMI])
  AC_SUBST([FORESTGOMP_NUMA_SUPPORT])

  $1="$HAVE_MARCEL_MAMI"

  AM_CONDITIONAL([HAVE_MARCEL_MAMI], [test "x$have_marcel_mami" = "xyes"])
])

dnl FGOMP_DETERMINE_CACHE_CHARACTERISTICS
dnl
dnl Define CPP macros that contain information about the top-level
dnl cache, i.e., the one that's shared among the greatest number of
dnl cores.
AC_DEFUN([FGOMP_DETERMINE_CACHE_CHARACTERISTICS], [
  AC_MSG_CHECKING([whether cache characteristics can be determined])

  # Try using Linux' sysfs to determine the cache characteristics.
  if test -d "/sys/devices/system/cpu/cpu0/cache"; then
    AC_MSG_RESULT([yes])

    AC_MSG_CHECKING([whether the top-most cache can be found])

    # Look for the highest (top-level) cache, i.e., the one that's
    # shared among as many cores as possible.
    max_level=0
    top_level_cache=""
    for cache in /sys/devices/system/cpu/cpu0/cache/index@<:@0-9@:>@
    do
      if test $(cat "$cache/level") -gt $max_level; then
        top_level_cache="$cache"
      fi
    done

    if test "x$top_level_cache" != "x"; then
      AC_MSG_RESULT([yes])

      # Check the top-level cache's characteristics.
      AC_MSG_CHECKING([for the top-most cache level])
      top_level_cache_level="$(cat "$top_level_cache/level")"
      AC_MSG_RESULT([L$top_level_cache_level])

      AC_MSG_CHECKING([for top-level cache size])
      ac_fgomp_top_level_cache_size_KiB="$(cat "$top_level_cache/size" | grep -o '@<:@0-9@:>@\+')"
      ac_fgomp_top_level_cache_size="$(expr "$ac_fgomp_top_level_cache_size_KiB" \* 1024)"
      AC_MSG_RESULT([$ac_fgomp_top_level_cache_size])

      AC_MSG_CHECKING([for top-level cache line size])
      ac_fgomp_top_level_cache_line_size="$(cat "$top_level_cache/coherency_line_size")"
      AC_MSG_RESULT([$ac_fgomp_top_level_cache_line_size])

      AC_MSG_CHECKING([for the number of cores associated with the top-level cache])
      if test -f "$top_level_cache/shared_cpu_list"; then
        ac_fgomp_top_level_cache_core_count="$(		\
	  cat "$top_level_cache/shared_cpu_list" |	\
	  grep -o '@<:@0-9@:>@\+' |			\
	  ( read lo ; read hi ;				\
	    if test "x$hi" = "x"; then echo 1;		\
	    else expr $hi - $lo + 1; fi ))"
	AC_MSG_RESULT([$ac_fgomp_top_level_cache_core_count])
	if test "$ac_fgomp_top_level_cache_core_count" -lt 2; then
	  AC_MSG_WARN([suspiciously low number of cores sharing the top-level cache])
	fi
      else
        AC_MSG_RESULT([unknown])
	ac_fgomp_top_level_cache_core_count=2
      fi
    fi
  else
    AC_MSG_RESULT([no])
    AC_MSG_WARN([don't know how to determine cache characteristics])
    AC_MSG_WARN([using default cache values])
  fi

  if test "x$ac_fgomp_top_level_cache_size" = "x"; then
    # "Sane" defaults per today's standards.
    ac_fgomp_top_level_cache_size=524288  # 512 KiB
    ac_fgomp_top_level_cache_line_size=64
  fi

  AC_DEFINE_UNQUOTED([TOP_LEVEL_CACHE_SIZE], [$ac_fgomp_top_level_cache_size],
    [Define to the size of the top-most cache])
  AC_DEFINE_UNQUOTED([TOP_LEVEL_CACHE_LINE_SIZE], [$ac_fgomp_top_level_cache_line_size],
    [Define to the cache line size of the top-most cache])
  AC_DEFINE_UNQUOTED([TOP_LEVEL_CACHE_CORE_COUNT],
    [$ac_fgomp_top_level_cache_core_count],
    [Define to the number of cores sharing the top-level cache])
])

dnl FGOMP_DETERMINE_NUMBER_OF_CORES
dnl
dnl Determine the total number of cores, using Linux' sysfs.
AC_DEFUN([FGOMP_DETERMINE_NUMBER_OF_CORES], [
  AC_MSG_CHECKING([whether the number of cores can be determined])
  if test -d "/sys/devices/system/cpu"; then
    AC_MSG_RESULT([yes])
    AC_MSG_CHECKING([for the total number of cores])
    ac_fgomp_total_core_count="$(		\
      cd /sys/devices/system/cpu ;		\
      echo cpu@<:@0-9@:>@* | wc -w)"
    AC_MSG_RESULT([$ac_fgomp_total_core_count])
  else
    AC_MSG_RESULT([no])
    ac_fgomp_total_core_count=16
  fi

  AC_DEFINE_UNQUOTED([TOTAL_CORE_COUNT], [$ac_fgomp_total_core_count],
    [Define to the total number of cores])
])

dnl FGOMP_CHECK_PUKABI VAR
dnl
dnl Check whether PukABI (for binary compatibility with Glibc) is
dnl available.  Set VAR to `yes' or `no' depending on whether it's
dnl available and usable.  Substitute `HAVE_PUKABI', which is set to
dnl "yes" or "no".
AC_DEFUN([FGOMP_CHECK_PUKABI], [
  AC_REQUIRE([AC_CANONICAL_HOST])

  case "x$host" in
    x*-gnu)
      # Assume we can use `$LD_PRELOAD'.
      AC_CHECK_LIB([PukABI], [puk_abi_preinit],
	[ac_fgomp_have_pukabi=yes], [ac_fgomp_have_pukabi=no])

      if test "x$ac_fgomp_have_pukabi" != "xyes"; then
        AC_MSG_WARN([PukABI (part of Padico) is not available but is highly recommended.  See http://padico.gforge.inria.fr/.])
      fi
      ;;
    *)
      AC_MSG_WARN([don't know how to tell the loader to preload a library on this platform])
      ac_fgomp_have_pukabi=no
  esac

  if test "x$ac_fgomp_have_pukabi" = "xyes"; then
    AC_CHECK_HEADER([Padico/Puk-ABI.h],
      [AC_DEFINE([HAVE_PUKABI], [1],
         [Define to 1 if PukABI is available.])],
      [ac_fgomp_have_pukabi=no])
  fi

  AM_CONDITIONAL([HAVE_PUKABI], [test "x$ac_fgomp_have_pukabi" = "xyes"])

  HAVE_PUKABI="$ac_fgomp_have_pukabi"
  AC_SUBST([HAVE_PUKABI])

  $1="$ac_fgomp_have_pukabi"
])

dnl FGOMP_CHECK_DASH_MPOPCNT [ACTION-IF-SUPPORTED [ACTION-IF-NOT-SUPPORTED]]
dnl
dnl Check whether the compiler supports `-mpopcnt'.
AC_DEFUN([FGOMP_CHECK_DASH_MPOPCNT], [
  AC_CACHE_CHECK([whether the compiler supports -mpopcnt and __builtin_popcount and whether the CPU supports it],
    [ac_cv_have_dash_mpopcnt], [
    AC_REQUIRE([AC_CANONICAL_HOST])
    AC_REQUIRE([AC_PROG_CC])

    # Run the program to make sure the `popcnt' instruction is
    # implemented by the underlying CPU.
    #
    # Beware: the binary may not be usable on other CPUs of the same
    # architecture if they do not support that instruction.

    save_CFLAGS="$CFLAGS"
    CFLAGS="-mpopcnt $CFLAGS"
    AC_RUN_IFELSE([AC_LANG_PROGRAM([[]],
		    [[int x = __builtin_popcount ((int)(long)(void *) &main);
		      return (x > 1 ? 0 : 1);]])],
      [ac_cv_have_dash_mpopcnt=yes],
      [ac_cv_have_dash_mpopcnt=no],
      [ac_cv_have_dash_mpopcnt=no])
    CFLAGS="$save_CFLAGS"])

  if test "x$ac_cv_have_dash_mpopcnt" = "xyes"; then
     $1
     :
  else
     $2
     :
  fi])

dnl FGOMP_CHECK_OMP_LOCK_TYPES
dnl
dnl Check the size and alignment of `omp_lock_t' and `omp_nest_lock_t'
dnl from <omp.h>.  Create output variables and CPP macros containing
dnl the result.
AC_DEFUN([FGOMP_CHECK_OMP_LOCK_TYPES], [
  ac_fgomp_save_CFLAGS="$CFLAGS"
  ac_fgomp_save_LDFLAGS="$LDFLAGS"

  CFLAGS="$CFLAGS -fopenmp"
  LDFLAGS="$LDFLAGS -fopenmp"

  dnl First, get the result as output variables, for "omp.h.in" et al.
  AC_COMPUTE_INT([OMP_LOCK_SIZE], [sizeof (omp_lock_t)],
    [#include <omp.h>],
    [AC_MSG_ERROR([unsupported system, cannot find sizeof (omp_lock_t)])])
  AC_COMPUTE_INT([OMP_LOCK_ALIGN], [__alignof (omp_lock_t)],
    [#include <omp.h>])
  AC_COMPUTE_INT([OMP_NEST_LOCK_SIZE], [sizeof (omp_nest_lock_t)],
    [#include <omp.h>])
  AC_COMPUTE_INT([OMP_NEST_LOCK_ALIGN], [__alignof (omp_nest_lock_t)],
    [#include <omp.h>])

  dnl Second, get the result as CPP macros (XXX: this is suboptimal.)
  AC_CHECK_SIZEOF([omp_lock_t],, [#include <omp.h>])
  AC_CHECK_ALIGNOF([omp_lock_t], [#include <omp.h>])
  AC_CHECK_SIZEOF([omp_nest_lock_t],, [#include <omp.h>])
  AC_CHECK_ALIGNOF([omp_nest_lock_t], [#include <omp.h>])

  CFLAGS="$ac_fgomp_save_CFLAGS"
  LDFLAGS="$ac_fgomp_save_LDFLAGS"

  AC_SUBST([OMP_LOCK_SIZE])
  AC_SUBST([OMP_LOCK_ALIGN])
  AC_SUBST([OMP_NEST_LOCK_SIZE])
  AC_SUBST([OMP_NEST_LOCK_ALIGN])
])

dnl FGOMP_CHECK_MARCEL_LOCK_TYPES
dnl
dnl Check the size and alignment of `marcel_mutex_t' and
dnl `marcel_recursivemutex_t'.  Create output variables and CPP macros
dnl containing the result.
AC_DEFUN([FGOMP_CHECK_MARCEL_LOCK_TYPES], [
  PM2_CHECK([
    AC_CHECK_SIZEOF([marcel_mutex_t],, [#include <marcel.h>])
    AC_CHECK_ALIGNOF([marcel_mutex_t], [#include <marcel.h>])
    AC_CHECK_SIZEOF([marcel_recursivemutex_t],, [#include <marcel.h>])
    AC_CHECK_ALIGNOF([marcel_recursivemutex_t], [#include <marcel.h>])
  ])

  dnl We also need to know about pointers.
  AC_CHECK_SIZEOF([void *])
  AC_CHECK_ALIGNOF([void *])
])
