/* Copyright (C) 2008-2009 INRIA

   This file is part of the GNU OpenMP Library (libgomp).

   Libgomp is free software; you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 2.1 of the License, or
   (at your option) any later version.

   Libgomp is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License 
   along with libgomp; see the file COPYING.LIB.  If not, write to the
   Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.  */

/* This program is meant to test cache behavior of our lock-free
   barriers.  */

#include <marcel.h>

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#ifdef HAVE_PAPI
# include <papi.h>
#endif

#include <bar.h>


/* Number of times threads will wait on the barrier.  */
#define ITERATION_COUNT 50000

struct thread_input
{
  gomp_barrier_t *barrier;
  unsigned int    index;
};

static void *
thread_entry_point (void *arg)
{
#ifdef HAVE_PAPI
#define EVENT_COUNT   5
  /* Often, machines have only 4 hardware counters.  The one that
     matters the most here is the number of cache misses of the
     "top-most" cache, so we try `PAPI_L3_TCM' and fall back to the
     other cache levels if no L3 is available.  */
  static int events[] = { PAPI_TOT_INS, PAPI_TOT_CYC, PAPI_L3_TCM, PAPI_L2_DCM, PAPI_L1_DCM };
  int available_events[EVENT_COUNT];
  int err, eventset = PAPI_NULL;
  long long hw_counters[EVENT_COUNT];
#endif

  unsigned i;
  const struct thread_input *input;

  marcel_thread_preemption_disable ();

  input = (struct thread_input *) arg;
  marcel_printf ("thread %02u, vp %i\n", input->index, marcel_current_vp ());

  /* Warm the caches.  */
  for (i = 0; i < 10; i++)
    gomp_barrier_wait (input->barrier, input->index);

#ifdef HAVE_PAPI
  if (input->index == 0)
    {
      PAPI_option_t option;

      err = PAPI_create_eventset (&eventset);
      assert (err == PAPI_OK);

      /* Collect counters for user-level code only.  */
      option.domain.domain = PAPI_DOM_USER;
      option.domain.eventset = eventset;
      err = PAPI_set_opt (PAPI_DOMAIN, &option);
      assert (err == PAPI_OK);

      for (i = 0; i < EVENT_COUNT; i++)
	{
	  err = PAPI_add_event (eventset, events[i]);
	  available_events[i] = (err == PAPI_OK);
	}

      err = PAPI_start (eventset);
      assert (err == PAPI_OK);
    }
#endif

  for (i = 0; i < ITERATION_COUNT; i++)
    gomp_barrier_wait (input->barrier, input->index);

#ifdef HAVE_PAPI
  if (input->index == 0)
    {
      err = PAPI_stop (eventset, hw_counters);
      assert (err == PAPI_OK);

      for (i = 0; i < EVENT_COUNT; i++)
	{
	  if (available_events[i])
	    {
	      char name[512];

	      err = PAPI_event_code_to_name (events[i], name);
	      assert (err == PAPI_OK);
	      marcel_printf ("%10s ...... %12lli\n", name, hw_counters[i]);
	    }
	}
    }
#endif

  return NULL;
}


int
main (int argc, char *argv[])
{
  gomp_barrier_t bar;
  unsigned i, thread_count;

  marcel_init (&argc, argv);
  marcel_ensure_abi_compatibility (MARCEL_HEADER_HASH);

  marcel_thread_preemption_disable ();

#ifdef HAVE_PAPI
  int err;

  err = PAPI_library_init (PAPI_VER_CURRENT);
  assert ((err == PAPI_VER_CURRENT) || (err == 0));
#endif

  /* Arrange to launch one thread per core.  */
  thread_count = ma_nbprocessors ();
  marcel_printf ("%u barriers among %u threads\n",
	  ITERATION_COUNT, thread_count);

  gomp_barrier_init (&bar, thread_count, "bar");

  {
    marcel_t threads[thread_count];
    struct thread_input thread_input[thread_count];

    threads[0] = marcel_self ();

    thread_input[0].barrier = &bar;
    thread_input[0].index = 0;

    for (i = 1; i < thread_count; i++)
      {
	marcel_attr_t attrs;
	marcel_vpset_t vpset;

	marcel_vpset_init (&vpset);
	marcel_vpset_set (&vpset, i);
	marcel_attr_init (&attrs);
	marcel_attr_setvpset (&attrs, vpset);

	thread_input[i].barrier = &bar;
	thread_input[i].index = i;
	marcel_create (&threads[i], &attrs,
		       thread_entry_point, &thread_input[i]);
      }

    thread_entry_point (&thread_input[0]);

    for (i = 1; i < thread_count; i++)
      marcel_join (threads[i], NULL);
  }

  gomp_barrier_destroy (&bar);

  marcel_end ();

  return 0;
}
