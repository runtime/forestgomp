/* Copyright (C) 2008 INRIA
   Originally written by François Broquedis <francois.broquedis@labri.fr>.

   This file is part of the GNU OpenMP Library (libgomp).

   Libgomp is free software; you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 2.1 of the License, or
   (at your option) any later version.

   Libgomp is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with libgomp; see the file COPYING.LIB.  If not, write to the
   Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.  */


/* The thread represented by _my_index_ marks the barrier and tries to
   get higher in the binary tree. The higher reached node is
   returned. */

/* Needed to use Marcel's memory barrier primitives.  */
#define MARCEL_INTERNAL_INCLUDE

#include <libgomp.h>
#include <malloc.h>
#include "bar.h"


#if (defined FGOMP_DEBUG) && (defined FGOMP_LOCK_FREE_DEBUGGING)
# include <stdlib.h>
# include <stdio.h>
# include <signal.h>
# ifdef __GNU_LIBRARY__
#  include <execinfo.h>
# endif
#endif


/* Helper macros to manipulate binary trees and barriers.  */

/* Return true if index ID denotes the root of a binary tree.  */
#define bintree_root_p(bar, id, step)   (step == (bar)->bintree_height)

/* Return the column of ID's sibling in a binary tree.  */
#define bintree_sibling_column(id, step)   (2*(step) + (((id)&(1<<(step)))==0)) 

/* Direct access to a line of a binary tree. */
#define bintree_line(id, step) ((id/2)&(~((1<<(step))-1))) 

/* Direct access to a column of a binary tree. */
#define bintree_column(id, step)  (2*(step) + (((id)&(1<<(step)))!=0))

/* Provide access to node ID of BAR's binary tree for CYCLE.  */
#define barrier_node(bar, cycle, id, step)				\
  ((volatile bintree_t *) (&(bar)->bintree_buffer			\
			   [bintree_line((id),(step)) * CACHE_LINE_SIZE +  bintree_column((id), (step))]))

/* Provide access to ID's sibling of BAR's binary tree for CYCLE.  */
#define barrier_sibling_node(bar, cycle, id, step)				\
  ((volatile bintree_t *) (&(bar)->bintree_buffer			\
			   [bintree_line((id),(step)) * CACHE_LINE_SIZE +  bintree_sibling_column((id), (step))]))

/* Per-barrier per-thread data structure. */
typedef struct thread_data
{
  char released;
  char retry;
  char cycle;
  unsigned bindex;
  unsigned lastvp;
} * thread_data;

/* Provide access to the ID thread_data structure of barrier BAR. */
#define thr_data(bar,id) ((thread_data)((bar)->per_thread_data + (id) * CACHE_LINE_SIZE))




static inline void *
fgomp_memalign (size_t boundary, size_t size)
{
  void *result;

  marcel_extlib_protect ();

  /* BOUNDARY must be a power of two.  */
  assert (__builtin_popcount (boundary) == 1);
  result = memalign (boundary, size);

  marcel_extlib_unprotect ();

  debug ("align %p %d \n", result, (int) boundary);

  return result;
}

static inline void
fgomp_memalign_free (void *ptr)
{
#ifdef __GLIBC__
  /* The glibc doc (info "(libc) Aligned Memory Blocks") mentions that
     `memalign'ed blocks can be freed with `free(3)' with glibc but not with
     BSD's libc.  */
  marcel_extlib_protect ();
  free (ptr);
  marcel_extlib_unprotect ();
#endif
}

#ifdef FGOMP_DEBUG

void
fgomp_show_barrier(fgomp_barrier_t *b)
{
  unsigned step, id;
  char current_cycle = b->cycle ;

  debug ("Barrier %s (%p) cycle:%d actual_count:%d \n", b->nickname, b, current_cycle, b->actual_count);

  for (id = 0; id < b->actual_count; id++)
    {
      struct thread_data td = *thr_data(b,id);
      debug ("\n thread %d \t released:%d retry:%d cycle:%d bindex:%d lastvp:%d \n",
	      id, td.released, td.retry, td.cycle, td.bindex, td.lastvp);

      for (step = 0; step < b->bintree_height; step++)
	debug ("%d|%d\t", *barrier_node (b, current_cycle, id, step), *barrier_sibling_node (b, current_cycle, id, step));
    }

  debug ("DONE\n");
}

#endif

static inline unsigned
fgomp_barrier_cross (fgomp_barrier_t *b, unsigned current_cycle, unsigned my_index)
{
  unsigned step;

  for (step = 0; step < b->bintree_height; step++)
    {
      *barrier_node (b, current_cycle, my_index, step) = current_cycle;

      /* This full memory barrier (i.e., read/write, aka. "mfence")
	 guarantees that even if we see an old value from our sibling, our
	 sibling will see our new value. */
       ma_mb ();

      if ((* barrier_sibling_node (b, current_cycle, my_index, step) & current_cycle) == 0)
	return 0;
    }
  
  return 1;
}

/* If X is not a power of 2, return the immediately higher power of two;
   otherwise return X.  */
#define POWER2_ROUND_UP(x)				\
  (((x) == 0)						\
   ? 1U							\
   : ((__builtin_popcount (x) == 1)			\
      ? (x)						\
      : (1U << (sizeof (x) * 8 - __builtin_clz (x)))))

/* Changes B's cycle from OLD_CYCLE to the next cycle, unless that has
   already been done.  */
static inline void
fgomp_barrier_change_cycles (fgomp_barrier_t *b, unsigned old_cycle)
{
  debug ("reinitializing %s (%p), old_cycle was %d, the new on is %d\n", 
	 b->nickname, b, old_cycle, old_cycle ^ 3);
  if (old_cycle == b->cycle) 
    {
      ma_wmb(); 
      b->cycle = old_cycle ^ 3;
      ma_mb(); 
      
    }
}

/* Redistribute ForestGOMP threads indexes according the way they are
   currently scheduled. */
static int
do_index_distribution (fgomp_barrier_t *b, unsigned int current_cycle)
{
  unsigned nproc = marcel_nbprocessors;
  int next_index[nproc+1];
  int i;

  if (marcel_mutex_trylock (&b->redistribute_mutex))
    { 
      if (b->bad_distribution != 0)
	{
	  for (i = 0; i < b->actual_count; i++) {
	    while (thr_data (b, i)->released == 1) {
              gomp_team_process_uninstantiated_tasks ();
              marcel_yield ();
            }
	  }
	  
	  memset (next_index,0, nproc*sizeof(int));
	  
	  for (i = 0; i < b->actual_count; i++)
	    next_index[thr_data (b,i)->lastvp]++;
	  
	  next_index[nproc] = b->actual_count ;
	  
	  for (i = nproc - 1; i >= 0; i--)
	    next_index[i] = next_index[i+1] - next_index[i];
	  
	  for (i = 0; i < b->actual_count; i++)
	    {
	      debug ("thr %d sur %d index %d \n", i, thr_data (b,i)->lastvp,next_index[thr_data (b,i)->lastvp]);
	      thr_data (b,i)->bindex = next_index[thr_data (b,i)->lastvp]++;
	    }
	  
	  b->bad_distribution = 0;
	  fgomp_barrier_change_cycles (b, current_cycle);
	}

      marcel_mutex_unlock (&b->redistribute_mutex);
      return 1;
    }

  return 0;
}

/* Initialize a ForestGOMP barrier. */
static inline void 
fgomp_barrier_init (fgomp_barrier_t *b, unsigned count, unsigned cycle,
		    char *nickname)
{
  int i;

  b->cycle = cycle;
  b->count = POWER2_ROUND_UP (count);
  b->actual_count = count;
  b->bintree_height =  ma_generic_fls (count - 1);

  marcel_mutex_init (&b->redistribute_mutex, NULL);

  b->bintree_buffer = fgomp_memalign (CACHE_LINE_SIZE, 2 * b->count *CACHE_LINE_SIZE);
  memset (b->bintree_buffer, 0, 2 * b->count * CACHE_LINE_SIZE);
  
  b->per_thread_data = fgomp_memalign (CACHE_LINE_SIZE, count * CACHE_LINE_SIZE);
  memset (b->per_thread_data, 0, count * CACHE_LINE_SIZE);
  
  b->bad_distribution = 0;

  for(i = 0; i < b->actual_count; i++)
    thr_data (b,i)->bindex = i;
    
  if (b->actual_count != b->count)
    {
      for (i = b->actual_count; i < b->count; i++)
	{
	  /* Set the fake bintree nodes (those added to handle a
	     number of threads that is not a power of 2) to 3 (3 means
	     "not partipating") */
	  fgomp_barrier_cross (b, 3, i);
	}
    }

  b->nickname = nickname;

  debug ("%s (%p) initialized at count %d\n", b->nickname, b, count);
}

/* Wait until C is true.  */
#define wait_until(_c)						\
  do								\
    {								\
      while (!(_c))						\
 	{							\
	  gomp_team_process_uninstantiated_tasks ();		\
	  marcel_yield ();					\
	  ma_barrier ();					\
	}							\
    }								\
  while (0)

/* Wait until everyone has crossed barrier B. */
static inline void
fgomp_barrier_wait (fgomp_barrier_t *b, unsigned my_index) 
{
  if (b->actual_count == 1)
    return;

  unsigned reached, current_cycle;
  unsigned cvp = marcel_current_vp();

  /* TODO: The level we consider here should be matching the topology
     we're running on. (i.e., redistribute the indexes when accessing
     a different cache level, or only when accessing a different node
     for example) */
  if (cvp != thr_data (b, my_index)->lastvp)
    {
      thr_data (b, my_index)->lastvp = cvp;
 
      b->bad_distribution++;
    }

 retry:
  thr_data (b, my_index)->released = 1;
  current_cycle = b->cycle;
  thr_data (b, my_index)->cycle = current_cycle;
  
  debug ("thread %d (%p) waiting on cycle %d of `%s' (%p, count = %u)\n",
	 my_index, marcel_self (), current_cycle, b->nickname, b, b->count);
 
  reached = fgomp_barrier_cross (b, current_cycle, thr_data (b, my_index)->bindex);
 
  thr_data (b, my_index)->released = 2;
 
  if (reached && current_cycle ==  b->cycle)
    {
      if (b->bad_distribution) 
	{
	  if (!do_index_distribution (b, current_cycle))
	    wait_until (current_cycle != b->cycle);
	}
      else
	fgomp_barrier_change_cycles (b, current_cycle);
    }
  else 
    wait_until (current_cycle != b->cycle);

  debug ("thread %d (%p) leaving cycle %d of `%s' (%p, count = %u)\n",
	 my_index, marcel_self (), current_cycle, b->nickname, b, b->count);

  if (my_index >= b->actual_count)
    return;

  if (thr_data (b, my_index)->retry)
    {
      thr_data (b, my_index)->retry = 0;
      debug ("thread 0 (%p) of barrier `%s' (%p) crossed it for retry\n",
	     marcel_self (), b->nickname, b);
      goto retry;
    }
  
  thr_data (b, my_index)->released = 0;  
}

/* Cross barrier B without waiting on it. */ 
static inline bool
fgomp_barrier_wait_start (fgomp_barrier_t *b, unsigned my_index, unsigned int barrier_type) 
{
  unsigned reached;
  unsigned current_cycle = b->cycle;
  unsigned cvp = marcel_current_vp ();

  if (cvp != thr_data (b,my_index)->lastvp)
    {
      thr_data (b, my_index)->lastvp = cvp;
      b->bad_distribution++;
    }

  thr_data (b, my_index)->released = 1;  
  thr_data (b, my_index)->cycle = current_cycle;

  debug ("thread %d (%p) is getting through cycle %d of `%s' (%p)\n",
	 my_index, marcel_self (), current_cycle, b->nickname, b);

  reached = fgomp_barrier_cross (b, current_cycle, thr_data(b,my_index)->bindex);

  thr_data (b, my_index)->released = 2;

  if (reached)
    {
      /* FIXME: Adapt the following to match fgomp_barrier_wait (). */
      if (b->bad_distribution)
	do_index_distribution (b, current_cycle);
      fgomp_barrier_change_cycles (b, current_cycle);
    }

  if (barrier_type == FGOMP_BARRIER_NO_WAIT_END)
    thr_data (b, my_index)->released = 0;  

   return reached;
}

/* Checks whether everyone has crossed barrier B, waiting if
   necessary. */
static inline void
fgomp_barrier_wait_end (fgomp_barrier_t * b, unsigned my_index, bool last) 
{
  unsigned current_cycle = thr_data (b, my_index)->cycle;
  
  /* TODO: use wait_until () in here. */
  while (current_cycle == b->cycle) 
    {
      gomp_team_process_uninstantiated_tasks();
      marcel_yield ();
      ma_barrier ();
    }
 
  debug ("thread %d (%p) leaving cycle %d of `%s' (%p, count = %u)\n",
	 my_index, marcel_self (), current_cycle, b->nickname, b, b->count);
  
  if (thr_data (b, my_index)->retry)
    {
      thr_data (b, my_index)->retry = 0;
      ma_mb ();
      fgomp_barrier_wait (b, my_index);
      return;
    }
  else
    debug ("thread 0 (%p) of barrier `%s' (%p) crossed it for retry\n",
	   marcel_self (), b->nickname, b);

  thr_data (b, my_index)->released = 0;
}


/* Return true if all threads subscribed to B have left it.  */
static inline bool
barrier_released (gomp_barrier_t *b)
{
  unsigned i;

  ma_mb ();

  for (i = 0; i < b->actual_count; i++)
    if (thr_data (b, i)->released)
      return false; 
  
  return true;
}

/* Destroy a ForestGOMP barrier. */
static inline void 
fgomp_barrier_destroy (fgomp_barrier_t *b, bool wait)
{
  unsigned cycle;
  bintree_t *tree;
  char *per_thread_data;

  cycle = b->cycle;
  
  if (wait)
    /* Wait for all threads currently waiting on B.  */
    {
      while (!barrier_released (b)) {
        gomp_team_process_uninstantiated_tasks();
        marcel_yield ();
      }
    }

  tree = b->bintree_buffer;
  b->bintree_buffer = NULL;
  fgomp_memalign_free (tree);

  per_thread_data = b->per_thread_data;
  b->per_thread_data = NULL;
  fgomp_memalign_free (per_thread_data);

  debug ("destroyed barrier `%s' (%p)\n",
	 b->nickname, b);
}

void
gomp_barrier_init (gomp_barrier_t * bar, unsigned count, char *nickname)
{
  fgomp_barrier_init (bar, count, 1, nickname);
}

/* Refresh, and eventually resize, the barrier BAR. */
void
gomp_barrier_reinit (gomp_barrier_t * bar, unsigned count, char *nickname, bool last_alive_thread)
{
  debug ("reinitializing barrier `%s' (%p) from thread %p "
	 "with new nickname `%s' (count %u -> %u)\n",
	 bar->nickname, bar, marcel_self (),
	 nickname, bar->count, count);

  fgomp_barrier_t new_bar;
  bintree_t *prev_bintrees;
  char *prev_per_thread_data;
  unsigned int prev_cycle, prev_actual_count;
  unsigned step, i;
  
  prev_cycle = bar->cycle;
  prev_actual_count = bar->actual_count;
  prev_bintrees = bar->bintree_buffer;
  prev_per_thread_data = bar->per_thread_data;
  
  if (!last_alive_thread) 
    {
      /* The master thread has to wait for everybody to reach the barrier. */
      for (step = 0; step < bar->bintree_height ; step++)
	{
	  while ((* barrier_sibling_node (bar, prev_cycle, thr_data (bar, 0)->bindex, step) & prev_cycle) == 0)
	    ;
	}
    }
  
  /* Create a new, enlarged barrier, and subscribe the threads previously
     subscribed to BAR.  */
  fgomp_barrier_init (&new_bar, count, prev_cycle ^ 3, bar->nickname);
  
  if (!last_alive_thread)
    {
      /* Ask the threads to restart the current barrier. */
      for (i = 1; i < (count < prev_actual_count ? count : prev_actual_count); i++)
	thr_data (&new_bar, i)->retry = 1;
    }

  /* The cycle must be changed last here. */      
  new_bar.cycle = prev_cycle;
  *bar = new_bar;
  bar->cycle = prev_cycle ^ 3;
  
  /* We can now free the old barrier fields.  */
  fgomp_memalign_free (prev_bintrees);
  fgomp_memalign_free (prev_per_thread_data);
}

void
gomp_barrier_destroy (gomp_barrier_t * bar)
{
  /* Before destroying, make sure all threads have left the barrier.  */
  fgomp_barrier_destroy (bar, true);
}

void
gomp_barrier_wait (gomp_barrier_t * bar, unsigned my_index)
{  
#ifdef FGOMP_STATS
  if (my_index == 0)
    write_statistics (&fgomp_stats, FGOMP_STATS_BARRIER);
#endif
  
#ifdef PROFILE
  FGOMP_EVENT_GOMP_BAR_ENTER ()
#endif

  fgomp_barrier_wait (bar, my_index);

#ifdef PROFILE
  FGOMP_EVENT_GOMP_BAR_LEAVE ()
#endif  
}

bool
gomp_barrier_wait_start (gomp_barrier_t * bar, unsigned my_index, unsigned int barrier_type)
{
  return fgomp_barrier_wait_start (bar, my_index, barrier_type);
}

void
gomp_barrier_wait_end (gomp_barrier_t * bar, unsigned my_index, bool last)
{
  fgomp_barrier_wait_end (bar, my_index, last);
}

bool 
gomp_barrier_is_available (gomp_barrier_t * bar)
{
  return barrier_released (bar);
}

#if (defined FGOMP_DEBUG) && (defined FGOMP_LOCK_FREE_DEBUGGING)

char fgomp_debugging_messages[FGOMP_MAX_MESSAGE_COUNT][FGOMP_MAX_MESSAGE_SIZE];

int fgomp_debugging_message_number = 0;

static void
dump_debugging_messages (void)
{
  int i;

  marcel_fprintf (stderr, "\n%i debugging messages\n",
	   fgomp_debugging_message_number);
  for (i = 0; i < fgomp_debugging_message_number; i++)
    {
      if (i < FGOMP_MAX_MESSAGE_COUNT)
	marcel_fprintf (stderr, fgomp_debugging_messages[i]);
      else
	break;
    }

  if (fgomp_debugging_message_number >= FGOMP_MAX_MESSAGE_COUNT)
    marcel_fprintf (stderr, "\nWarning: Debugging messages possibly lost!\n");
}

/* Obtain a backtrace and print it to `stderr'. */
static void
maprint_stack_trace (bool show_symbols)
{
#ifdef __GNU_LIBRARY__
#define TRACE_DEPTH 20
  void *array[TRACE_DEPTH];
  size_t size;
  size_t i;

  size = backtrace (array, TRACE_DEPTH);

  marcel_fprintf (stderr, "\nobtained %zd stack frames\n", size);

  if (show_symbols)
    {
      char **strings;

      strings = backtrace_symbols (array, size);

      for (i = 0; i < size; i++)
	marcel_fprintf (stderr, "  %s\n", strings[i]);

      free (strings);
    }
  else
    {
      for (i = 0; i < size; i++)
	marcel_fprintf (stderr, "  %p\n", array[i]);
    }
#undef TRACE_DEPTH
#endif
}

static void
handle_signal (int signum)
{
  /* XXX: We shouldn't call `printf ()' from a signal handler but it doesn't
     hurt to give it a try.  */
  marcel_fprintf (stderr, "\ncaught signal %i\n", signum);

  dump_debugging_messages ();
  print_stack_trace (signum != SIGSEGV);

  _exit (signum);
}

static void __attribute__ ((__constructor__))
initialize_debugging (void)
{
  atexit (dump_debugging_messages);
  signal (SIGINT, handle_signal);
  signal (SIGSEGV, handle_signal);
  signal (SIGABRT, handle_signal);
}
#endif
