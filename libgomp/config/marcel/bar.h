/* Copyright (C) 2005 Free Software Foundation, Inc.
   Contributed by Richard Henderson <rth@redhat.com>.

   This file is part of the GNU OpenMP Library (libgomp).

   Libgomp is free software; you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 2.1 of the License, or
   (at your option) any later version.

   Libgomp is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License 
   along with libgomp; see the file COPYING.LIB.  If not, write to the
   Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.  */

/* As a special exception, if you link this library with other files, some
   of which are compiled with GCC, to produce an executable, this library
   does not by itself cause the resulting executable to be covered by the
   GNU General Public License.  This exception does not however invalidate
   any other reasons why the executable file might be covered by the GNU
   General Public License.  */

/* This is the default implementation of a barrier synchronization mechanism
   for libgomp.  This type is private to the library.  Note that we rely on
   being able to adjust the barrier count while threads are blocked, so the
   POSIX pthread_barrier_t won't work.  */

#ifndef GOMP_BARRIER_H
#define GOMP_BARRIER_H 1

#ifdef HAVE_ATTRIBUTE_VISIBILITY
# pragma GCC visibility push(default)
#endif
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include <numaif.h>

#ifdef HAVE_ATTRIBUTE_VISIBILITY
# pragma GCC visibility pop
#endif

/* Constants passed to gomp_barrier_wait_start () to decide whether
   gomp_barrier_wait_end () should be called to complete the
   barrier. */
#define FGOMP_BARRIER_NEED_WAIT_END     0
#define FGOMP_BARRIER_NO_WAIT_END       1

#ifdef FGOMP_DEBUG
# ifdef FGOMP_LOCK_FREE_DEBUGGING

/* Provide a "lock-free" `debug' primitive.  Instead of doing I/O right when
   it's called, the primitive store the debugging message in a static array.
   Debugging messages are then only dumped upon SIGINT or `exit ()'.  */

#  define FGOMP_MAX_MESSAGE_SIZE   256
#  define FGOMP_MAX_MESSAGE_COUNT 1024

/* The buffer holding debugging messages.  */
extern char
fgomp_debugging_messages[FGOMP_MAX_MESSAGE_COUNT][FGOMP_MAX_MESSAGE_SIZE];

/* Index of the current debugging message.  */
extern int fgomp_debugging_message_number;

#  define debug(fmt, ...)						\
  do									\
    {									\
      int _msg;								\
      _msg = __sync_fetch_and_add (&fgomp_debugging_message_number, 1);	\
      if (_msg < FGOMP_MAX_MESSAGE_COUNT)				\
	snprintf (fgomp_debugging_messages[_msg], FGOMP_MAX_MESSAGE_SIZE, \
		  fmt, ##__VA_ARGS__);					\
    }									\
  while (0)

# else  /* !FGOMP_LOCK_FREE_DEBUGGING */

/* Provide a `debug' primitive that writes messages when it's invoked.  Doing
   I/O potentially involves locking and slows down the whole process, which
   may have affect the program's behavior.  */

#  define debug(fmt, ...) marcel_fprintf (stderr, fmt, ##__VA_ARGS__)

# endif /* !FGOMP_LOCK_FREE_DEBUGGING */

#else  /* !FGOMP_DEBUG */
# define debug(fmt, ...) (void)0
//#define debug(fmt, ...) marcel_fprintf (stderr, fmt, ##__VA_ARGS__)
#endif /* !FGOMP_DEBUG */


/* Type of binary tree nodes.  */
typedef char bintree_t;

/* Number of barrier "cycles", i.e., number of binary trees per barrier.  */
#define FGOMP_BARRIER_CYCLE_COUNT  (3)

extern bool fgomp_lonesome_team (void);


#define CACHE_LINE_SIZE (64)

typedef struct 
{
  /* Buffer representing the binary tree */
  bintree_t *bintree_buffer;

  /* height of the bintree */
  int bintree_height; 

  /* Current cycle */
  unsigned cycle;

  /* Number of participating threads, rounded up to a power of two */
  unsigned count;

  /* Actual number of participating threads, i.e., not rounded */
  unsigned actual_count;

  /* number of threads which do not have the right index to get optimal perf*/
  unsigned bad_distribution;

  /* Array indicating for each participating thread whether it is
     released,  whether it should re-block on the barrier once
     it has crossed it, and cycle for the 2-step barrier  */
  bintree_t *per_thread_data;

  /* Mutex to make sure there can be only one thread to perform the
     indexes new distribution. */
  marcel_mutex_t redistribute_mutex;

  /* Barrier nickname! (mostly used while debugging) */ 
  char *nickname;

} fgomp_barrier_t;

typedef fgomp_barrier_t gomp_barrier_t;

void gomp_barrier_init (gomp_barrier_t * bar, unsigned count, char *nickname);
void gomp_barrier_reinit (gomp_barrier_t * bar, unsigned count, char *nickname, bool last_alive_thread);
void gomp_barrier_destroy (gomp_barrier_t * bar);
void gomp_barrier_wait (gomp_barrier_t * bar, unsigned my_index);
bool gomp_barrier_wait_start (gomp_barrier_t * bar, unsigned my_index, unsigned int barrier_type);
void gomp_barrier_wait_end (gomp_barrier_t * bar, unsigned my_index, bool last);
bool gomp_barrier_is_available (gomp_barrier_t * bar);

#endif /* GOMP_BARRIER_H */
