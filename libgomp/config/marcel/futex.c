/* Copyright (C) 2008 INRIA
   Originally written by François Broquedis <francois.broquedis@labri.fr>.

   This file is part of the GNU OpenMP Library (libgomp).

   Libgomp is free software; you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 2.1 of the License, or
   (at your option) any later version.

   Libgomp is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with libgomp; see the file COPYING.LIB.  If not, write to the
   Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.  */


/* The thread represented by _my_index_ marks the barrier and tries to
   get higher in the binary tree. The higher reached node is
   returned. */

/* Needed to use Marcel's atomic primitives.  */
#define MARCEL_INTERNAL_INCLUDE

#include <libgomp.h>
#include "futex.h"

void gomp_futex_init (gomp_futex_t *futex, unsigned long spin_threshold)
{
  futex->held = 0; 
  futex->futex = (marcel_futex_t)MARCEL_FUTEX_INITIALIZER;
  futex->spin_threshold = spin_threshold;
}

void gomp_futex_wait (gomp_futex_t *futex)
{
  int retries = 0;

  while (ma_test_and_set_bit (0, &futex->held))
    {
      retries++;
  
      if (retries > futex->spin_threshold)
	marcel_futex_wait (&futex->futex, &futex->held, 1, 1, MA_MAX_SCHEDULE_TIMEOUT);
    }
}

void gomp_futex_wake (gomp_futex_t *futex, unsigned int nb_threads)
{
  ma_clear_bit (0, &futex->held);
  marcel_futex_wake (&futex->futex, nb_threads);
}

