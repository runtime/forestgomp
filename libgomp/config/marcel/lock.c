/* Copyright (C) 2007, 2008, 2009 INRIA

   This file is part of the GNU OpenMP Library (libgomp).

   Libgomp is free software; you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 2.1 of the License, or
   (at your option) any later version.

   Libgomp is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with libgomp; see the file COPYING.LIB.  If not, write to the
   Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.  */

/* As a special exception, if you link this library with other files, some
   of which are compiled with GCC, to produce an executable, this library
   does not by itself cause the resulting executable to be covered by the
   GNU General Public License.  This exception does not however invalidate
   any other reasons why the executable file might be covered by the GNU
   General Public License.  */

/* This is the Marcel implementation of the public OpenMP locking
   primitives.  */


#include "libgomp.h"
#include <slab.h>
#include <assert.h>

/* The maximum size of the lock table.  */
#define LOCK_TABLE_SIZE  1024U

/* Produce support code for lock handling in cases where `omp_{nest_,}lock_t'
   is smaller than a pointer.  */
#define MAKE_LOCK_TABLE_CODE(kind, marcel_lock_t)		\
								\
  static marcel_lock_t *kind ## _table[LOCK_TABLE_SIZE];	\
  static unsigned int   kind ## _table_last_id;			\
								\
  static inline unsigned int					\
  kind ## _table_allocate (void)				\
  {								\
    unsigned int index;						\
    marcel_lock_t *lock;					\
								\
    lock = marcel_malloc (sizeof (*lock), __FILE__, __LINE__);	\
    assert (lock != NULL);					\
								\
   retry:							\
    for (index = kind ## _table_last_id;			\
	 kind ## _table[index] != NULL;				\
	 index = (index + 1) % LOCK_TABLE_SIZE);		\
								\
    if (__sync_bool_compare_and_swap (&kind ## _table[index],	\
				      NULL, lock))		\
      kind ## _table_last_id = index + 1;			\
    else							\
      goto retry;						\
								\
    return index;						\
  }								\
								\
  static inline void						\
  kind ## _table_deallocate (omp_lock_t lock)			\
  {								\
    marcel_lock_t *real_lock;					\
								\
    real_lock = kind ## _table[lock];				\
    assert (real_lock != NULL);					\
								\
    kind ## _table[lock] = NULL;				\
    marcel_free (real_lock);					\
  }



/* Flat locks.  */

#ifdef FGOMP_USE_OMP_LOCK_TABLE
MAKE_LOCK_TABLE_CODE (lock, marcel_mutex_t)
#endif

#ifdef FGOMP_USE_OMP_LOCK_INDIRECTION
static struct gomp_slab_space lock_slab_space =
  GOMP_SLAB_SPACE_INITIALIZER (marcel_mutex_t, NULL, NULL,
			       NULL, NULL, NULL);
#endif

void
omp_init_lock (omp_lock_t *lock)
{
#ifdef FGOMP_USE_OMP_LOCK_TABLE
  *lock = lock_table_allocate ();
#elif defined FGOMP_USE_OMP_LOCK_INDIRECTION
  error_t err;

  err = gomp_slab_alloc (&lock_slab_space, (void **) lock);
  assert (err == 0 && *lock != NULL);
#endif

  marcel_mutex_init (FGOMP_LOCK_REF (lock), NULL);
}

void
omp_destroy_lock (omp_lock_t *lock)
{
  marcel_mutex_destroy (FGOMP_LOCK_REF (lock));

#ifdef FGOMP_USE_OMP_LOCK_TABLE
  lock_table_deallocate (*lock);
#elif defined FGOMP_USE_OMP_LOCK_INDIRECTION
  gomp_slab_dealloc (&lock_slab_space, (void *) *lock);
  *lock = NULL;
#endif
}

void
omp_set_lock (omp_lock_t *lock)
{
  marcel_mutex_lock (FGOMP_LOCK_REF (lock));
}

void
omp_unset_lock (omp_lock_t *lock)
{
  marcel_mutex_unlock (FGOMP_LOCK_REF (lock));
}

/* Return zero if LOCK is successfully set, non-zero otherwise.  */
int
omp_test_lock (omp_lock_t *lock)
{
  return marcel_mutex_trylock (FGOMP_LOCK_REF (lock)) == 1;
}


/* Nested locks.  */

#ifdef FGOMP_USE_OMP_NEST_LOCK_TABLE
MAKE_LOCK_TABLE_CODE (nest_lock, marcel_recursivemutex_t)
#endif

#ifdef FGOMP_USE_OMP_NEST_LOCK_INDIRECTION
static struct gomp_slab_space nest_lock_slab_space =
  GOMP_SLAB_SPACE_INITIALIZER (marcel_recursivemutex_t, NULL, NULL,
			       NULL, NULL, NULL);
#endif

void
omp_init_nest_lock (omp_nest_lock_t *lock)
{
#ifdef FGOMP_USE_OMP_NEST_LOCK_TABLE
  *lock = nest_lock_table_allocate ();
#elif defined FGOMP_USE_OMP_NEST_LOCK_INDIRECTION
  error_t err;

  err = gomp_slab_alloc (&nest_lock_slab_space, (void **) lock);
  assert (err == 0 && *lock != NULL);
#endif

  marcel_recursivemutex_init (FGOMP_NEST_LOCK_REF (lock), NULL);
}

void
omp_destroy_nest_lock (omp_nest_lock_t *lock)
{
  marcel_recursivemutex_destroy (FGOMP_NEST_LOCK_REF (lock));

#ifdef FGOMP_USE_OMP_NEST_LOCK_TABLE
  nest_lock_table_deallocate (*lock);
#elif defined FGOMP_USE_OMP_NEST_LOCK_INDIRECTION
  gomp_slab_dealloc (&nest_lock_slab_space, (void *) *lock);
  *lock = NULL;
#endif
}

void
omp_set_nest_lock (omp_nest_lock_t *lock)
{
  marcel_recursivemutex_lock (FGOMP_NEST_LOCK_REF (lock));
}

void
omp_unset_nest_lock (omp_nest_lock_t *lock)
{
  marcel_recursivemutex_unlock (FGOMP_NEST_LOCK_REF (lock));
}

/* Return the new nesting count if LOCK is set, zero otherwise.  */
int
omp_test_nest_lock (omp_nest_lock_t *lock)
{
#if MARCEL_VERSION < 0x020002
/* r20910 of Marcel introduced an extended semantic for recursivemutex_trylock */
#error Please update PM2 to get the recursivemutex extented semantic.
#endif
  return marcel_recursivemutex_trylock (FGOMP_NEST_LOCK_REF (lock));
}

ialias (omp_init_lock)
ialias (omp_init_nest_lock)
ialias (omp_destroy_lock)
ialias (omp_destroy_nest_lock)
ialias (omp_set_lock)
ialias (omp_set_nest_lock)
ialias (omp_unset_lock)
ialias (omp_unset_nest_lock)
ialias (omp_test_lock)
ialias (omp_test_nest_lock)
