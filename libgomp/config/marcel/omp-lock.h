/* Copyright (C) 2007, 2008, 2009 INRIA

   This file is part of the GNU OpenMP Library (libgomp).

   Libgomp is free software; you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 2.1 of the License, or
   (at your option) any later version.

   Libgomp is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with libgomp; see the file COPYING.LIB.  If not, write to the
   Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.  */

/* Marcel-based implementation of `omp_lock_t' and `omp_nest_lock_t'.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <marcel.h>

/* We want to be binary compatible with the system's libgomp `omp_lock_t' and
   `omp_nest_lock_t' types.  This isn't easy because on GNU/Linux, for
   instance, these are implemented in terms of `int's manipulated with
   futex(2), but `sizeof (int)' is much smaller than `sizeof
   (marcel_mutex_t)'; furthermore, on x86_64-*-linux-gnu, `sizeof (int) <
   sizeof (void *)'.

   Thus, we have the following strategies:

     1. If the size and alignment of Marcel's lock types is smaller than or
        equal to that of libgomp, then we can use them directly.

     2. Otherwise, if the size of libgomp's lock types is greater than or
        equal to the pointer size, we add a simple indirection.

     3. Otherwise, our lock types are turned into integers, which represent
        an index within a lock table.

   The last solution is the most heavyweight.  */



/* Flat locks.  */

#if SIZEOF_OMP_LOCK_T >= SIZEOF_MARCEL_MUTEX_T		\
    && ALIGNOF_OMP_LOCK_T >= ALIGNOF_MARCEL_MUTEX_T

# define FGOMP_LOCK_REF(_lock)  (_lock)
typedef marcel_mutex_t omp_lock_t;

#elif SIZEOF_OMP_LOCK_T >= SIZEOF_VOID_P	\
      && ALIGNOF_OMP_LOCK_T >= ALIGNOF_VOID_P

# define FGOMP_USE_OMP_LOCK_INDIRECTION
# define FGOMP_LOCK_REF(_lock)  (*(_lock))
typedef marcel_mutex_t *omp_lock_t;

#else

# define FGOMP_USE_OMP_LOCK_TABLE
# define FGOMP_LOCK_REF(_lock)  (lock_table[*(_lock)])
typedef unsigned int omp_lock_t;

#endif



/* Nested locks.  */

#if SIZEOF_OMP_NEST_LOCK_T >= SIZEOF_MARCEL_RECURSIVEMUTEX_T		\
    && ALIGNOF_OMP_NEST_LOCK_T >= ALIGNOF_MARCEL_RECURSIVEMUTEX_T

# define FGOMP_NEST_LOCK_REF(_lock)  (_lock)
typedef marcel_recursivemutex_t omp_nest_lock_t;

#elif SIZEOF_OMP_NEST_LOCK_T >= SIZEOF_VOID_P		\
      && ALIGNOF_OMP_NEST_LOCK_T >= ALIGNOF_VOID_P

# define FGOMP_USE_OMP_NEST_LOCK_INDIRECTION
# define FGOMP_NEST_LOCK_REF(_lock)  (*(_lock))
typedef marcel_recursivemutex_t *omp_nest_lock_t;

#else

# define FGOMP_USE_OMP_NEST_LOCK_TABLE
# define FGOMP_NEST_LOCK_REF(_lock)  (nest_lock_table[*(_lock)])
typedef unsigned int omp_nest_lock_t;

#endif
