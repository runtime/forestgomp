/* Copyright (C) 2005 Free Software Foundation, Inc.
   Contributed by Richard Henderson <rth@redhat.com>.

   This file is part of the GNU OpenMP Library (libgomp).

   Libgomp is free software; you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 2.1 of the License, or
   (at your option) any later version.

   Libgomp is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License 
   along with libgomp; see the file COPYING.LIB.  If not, write to the
   Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.  */

/* As a special exception, if you link this library with other files, some
   of which are compiled with GCC, to produce an executable, this library
   does not by itself cause the resulting executable to be covered by the
   GNU General Public License.  This exception does not however invalidate
   any other reasons why the executable file might be covered by the GNU
   General Public License.  */

/* This is the default PTHREADS implementation of a mutex synchronization
   mechanism for libgomp.  This type is private to the library.  */

#ifndef GOMP_SPINLOCK_H
#define GOMP_SPINLOCK_H 1

typedef marcel_spinlock_t gomp_spinlock_t;

#define GOMP_SPINLOCK_INIT_0 0

static inline void gomp_spinlock_init (gomp_spinlock_t *spinlock)
{
  marcel_spin_init (spinlock, 0);
}

static inline void gomp_spinlock_lock (gomp_spinlock_t *spinlock)
{
  marcel_spin_lock (spinlock);
}

static inline void gomp_spinlock_unlock (gomp_spinlock_t *spinlock)
{
  marcel_spin_unlock (spinlock);
}

static inline void gomp_spinlock_destroy (gomp_spinlock_t *spinlock)
{
  marcel_spin_destroy (spinlock);
}

#endif /* GOMP_SPINLOCK_H */
