/* Copyright (C) 2008-2009 INRIA

   This file is part of the GNU OpenMP Library (libgomp).

   Libgomp is free software; you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 2.1 of the License, or
   (at your option) any later version.

   Libgomp is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License 
   along with libgomp; see the file COPYING.LIB.  If not, write to the
   Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.  */

/* As a special exception, if you link this library with other files, some
   of which are compiled with GCC, to produce an executable, this library
   does not by itself cause the resulting executable to be covered by the
   GNU General Public License.  This exception does not however invalidate
   any other reasons why the executable file might be covered by the GNU
   General Public License.  */

#ifndef FORESTGOMP_H
#define FORESTGOMP_H 1

/* Version string.  */
#define FORESTGOMP_VERSION       "@PACKAGE_VERSION@"

/* Greater than 0 if ForestGOMP comes with run-time NUMA support.  */
#define FORESTGOMP_NUMA_SUPPORT  @FORESTGOMP_NUMA_SUPPORT@



#if FORESTGOMP_NUMA_SUPPORT > 0

#include <marcel.h>
#include <mm_mami.h>

/* The ForestGOMP memory manager, needed by MaMI. */
extern mami_manager_t *fgomp_memory_manager;

/* Structure to describe a memory area attached to a gomp thread. */
struct fgomp_memory_area
{
  /* Beginning of the memory area. */
  void *buffer;
    
  /* TODO: Remove this and extract it at work share time. */
  size_t chunk_size;
};

typedef enum fgomp_shake_mode 
{
  FGOMP_SHAKE_WAIT,
  FGOMP_SHAKE_NOWAIT,
  FGOMP_DONT_SHAKE
} fgomp_shake_mode_t;

/* Allocates size_t bytes inside the fgomp memory manager. */
extern void *fgomp_malloc (size_t);

/* Allocates size_t bytes on a specific node. */
extern void *fgomp_malloc_on_node (size_t, unsigned int);

/* Mark the pages holding the buffer passed in argument to be migrated
   the next time a thread access them. */
extern int fgomp_migrate_on_next_touch (void *, size_t);

/* Mark the pages holding the buffer passed in argument to be attached
   to the next thread to access them. */
extern int fgomp_attach_on_next_touch (void *, size_t);

/* Specify that the upcoming parallel section will create threads
   working on "buffer". */
extern int fgomp_set_next_team_affinity (void *, size_t, fgomp_shake_mode_t);

/* Declare a buffer to be accessed by the current thread. The third
   argument decides whether each thread has to inform the bubble
   scheduler the memory affinity scheme has changed (no_wait), or only
   the main thread will pass the information (wait). */
extern int fgomp_set_current_thread_affinity (void *, size_t, fgomp_shake_mode_t);

/* Free a memory area allocated using fgomp_malloc/malloc_on_node. */
extern void fgomp_free (void *);

#endif /* FORESTGOMP_NUMA_SUPPORT */

/* Set the current ForestGOMP thread load. This helps the scheduler
   estimating how long the execution of the current thread job will
   last. */
extern void fgomp_set_current_thread_load (unsigned int);

/* Set the number of threads the caller will create the next time it
   goes parallel. */
extern void fgomp_set_num_threads (int);

/* Get the number of threads set by fgomp_set_num_threads (). If the
   caller never called fgomp_set_num_threads (), return
   omp_get_num_threads (). */
extern int fgomp_get_num_threads (void);

/* Return the location (the core id) of the calling thread. */
extern int fgomp_get_location (void);

/* Print the runqueue the caller is scheduled on. */
extern void fgomp_say_hi (const char *msg);

#endif /* FORESTGOMP_H */
