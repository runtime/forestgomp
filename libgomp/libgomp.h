/* Copyright (C) 2005 Free Software Foundation, Inc.
   Contributed by Richard Henderson <rth@redhat.com>.

   This file is part of the GNU OpenMP Library (libgomp).

   Libgomp is free software; you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 2.1 of the License, or
   (at your option) any later version.

   Libgomp is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License 
   along with libgomp; see the file COPYING.LIB.  If not, write to the
   Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.  */

/* As a special exception, if you link this library with other files, some
   of which are compiled with GCC, to produce an executable, this library
   does not by itself cause the resulting executable to be covered by the
   GNU General Public License.  This exception does not however invalidate
   any other reasons why the executable file might be covered by the GNU
   General Public License.  */

/* This file contains data types and function declarations that are not
   part of the official OpenMP user interface.  There are declarations
   in here that are part of the GNU OpenMP ABI, in that the compiler is
   required to know about them and use them.

   The convention is that the all caps prefix "GOMP" is used group items
   that are part of the external ABI, and the lower case prefix "gomp"
   is used group items that are completely private to the library.  */

#ifndef LIBGOMP_H 
#define LIBGOMP_H 1

#include "config.h"
#include "gstdint.h"
#include <stdbool.h>

/* profiling */

#ifdef PROFILE
#  define FUT_FGOMP_CODE	0xfd00

#  define FUT_FGOMP_PAPIMAR_EV0		FUT_FGOMP_CODE + 0x10
#  define FUT_FGOMP_PAPIMAR_EV1		FUT_FGOMP_CODE + 0x11
#  define FUT_FGOMP_PAPIMAR_EV2		FUT_FGOMP_CODE + 0x12
#  define FUT_FGOMP_PAPIMAR_EV3		FUT_FGOMP_CODE + 0x13
#  define FUT_FGOMP_MAX_PAPIMAR_EV	4

#  define FUT_FGOMP_GOMP_BAR_ENTER		FUT_FGOMP_CODE + 0x20
#  define FUT_FGOMP_GOMP_BAR_LEAVE		FUT_FGOMP_CODE + 0x21
#  define FUT_FGOMP_GOMP_TASK_WAIT_ENTER	FUT_FGOMP_CODE + 0x22
#  define FUT_FGOMP_GOMP_TASK_WAIT_LEAVE	FUT_FGOMP_CODE + 0x23
#  define FUT_FGOMP_GOMP_TASK_ENTER		FUT_FGOMP_CODE + 0x24
#  define FUT_FGOMP_GOMP_TASK_LEAVE		FUT_FGOMP_CODE + 0x25

#  define FGOMP_EVENT_PAPIMAR_EV(ev_num, ev_val)	FUT_DO_PROBE1(FUT_FGOMP_PAPIMAR_EV0+ev_num, ev_val);
#  define FGOMP_EVENT_PAPIMAR_EV0(ev_val)	FUT_DO_PROBE1(FUT_FGOMP_PAPIMAR_EV0, ev_val);
#  define FGOMP_EVENT_PAPIMAR_EV1(ev_val)	FUT_DO_PROBE1(FUT_FGOMP_PAPIMAR_EV1, ev_val);
#  define FGOMP_EVENT_PAPIMAR_EV2(ev_val)	FUT_DO_PROBE1(FUT_FGOMP_PAPIMAR_EV2, ev_val);
#  define FGOMP_EVENT_PAPIMAR_EV3(ev_val)	FUT_DO_PROBE1(FUT_FGOMP_PAPIMAR_EV3, ev_val);
#  define FGOMP_EVENT_GOMP_BAR_ENTER()		FUT_DO_PROBE0(FUT_FGOMP_GOMP_BAR_ENTER);
#  define FGOMP_EVENT_GOMP_BAR_LEAVE()		FUT_DO_PROBE0(FUT_FGOMP_GOMP_BAR_LEAVE);
#  define FGOMP_EVENT_GOMP_TASK_WAIT_ENTER(depth)	FUT_DO_PROBE1(FUT_FGOMP_GOMP_TASK_WAIT_ENTER,(depth));
#  define FGOMP_EVENT_GOMP_TASK_WAIT_LEAVE()	FUT_DO_PROBE0(FUT_FGOMP_GOMP_TASK_WAIT_LEAVE);
#  define FGOMP_EVENT_GOMP_TASK_ENTER(depth)		FUT_DO_PROBE1(FUT_FGOMP_GOMP_TASK_ENTER,(depth));
#  define FGOMP_EVENT_GOMP_TASK_LEAVE()		FUT_DO_PROBE0(FUT_FGOMP_GOMP_TASK_LEAVE);
#else
#  define FUT_FGOMP_MAX_PAPIMAR_EV		0

#  define FGOMP_EVENT_PAPIMAR_EV(ev_num, ev_val)
#  define FGOMP_EVENT_PAPIMAR_EV0(ev_val)
#  define FGOMP_EVENT_PAPIMAR_EV1(ev_val)
#  define FGOMP_EVENT_PAPIMAR_EV2(ev_val)
#  define FGOMP_EVENT_PAPIMAR_EV3(ev_val)
#  define FGOMP_EVENT_GOMP_BAR_ENTER()
#  define FGOMP_EVENT_GOMP_BAR_LEAVE()
#  define FGOMP_EVENT_GOMP_TASK_WAIT_ENTER(depth)
#  define FGOMP_EVENT_GOMP_TASK_WAIT_LEAVE()
#  define FGOMP_EVENT_GOMP_TASK_ENTER(depth)
#  define FGOMP_EVENT_GOMP_TASK_LEAVE()
#endif

/* error.c */

extern void gomp_error (const char *, ...)
	__attribute__((format (printf, 1, 2)));
extern void gomp_fatal (const char *, ...)
	__attribute__((noreturn, format (printf, 1, 2)));


#include <marcel.h>

#ifdef HAVE_PUKABI

/* When using PukABI, libc calls such as call to functions of the malloc
   family are protected by PukABI itself.  Thus, we must not use their
   `marcel_' variant since that would result in double-protection (i.e., two
   `marcel_extlib_protect ()' calls), which does not work.  */

# undef marcel_malloc
# define marcel_malloc(size, file, line)           malloc (size)
# undef marcel_calloc
# define marcel_calloc(count, size, file, line)    calloc ((count), (size))
# undef marcel_realloc
# define marcel_realloc(ptr, size, file, line)     realloc ((ptr), (size))
# undef marcel_free
# define marcel_free(ptr)                          free (ptr)

#endif /* HAVE_PUKABI */


#ifdef HAVE_ATTRIBUTE_VISIBILITY
# pragma GCC visibility push(hidden)
#endif

#include "sem.h"
#include "mutex.h"
#include "futex.h"
#include "spinlock.h"
#include "bar.h"
# include "stats.h"

#ifdef FGOMP_STATS

/* Structure to hold the ForestGOMP statistics (see stats.h for more
   details). */
extern fgomp_statistics_t fgomp_stats;

#endif /* FGOMP_STATS */

/* Determines wether there's currently only one team running. */
extern bool fgomp_lonesome_team (void);

/* This structure contains the data to control one work-sharing construct,
   either a LOOP (FOR/DO) or a SECTIONS.  */

enum gomp_schedule_type
{
  GFS_STATIC,
  GFS_DYNAMIC,
  GFS_GUIDED,
  GFS_RUNTIME
};

struct gomp_work_share
{
  /* This member records the SCHEDULE clause to be used for this construct.
     The user specification of "runtime" will already have been resolved.
     If this is a SECTIONS construct, this value will always be DYNAMIC.  */
  enum gomp_schedule_type sched;

  /* This is the chunk_size argument to the SCHEDULE clause.  */
  long chunk_size;

  /* This is the iteration end point.  If this is a SECTIONS construct, 
     this is the number of contained sections.  */
  long end;

  /* This is the iteration step.  If this is a SECTIONS construct, this
     is always 1.  */
  long incr;

  /* This is the index into the circular queue ordered_team_ids of the 
     current thread that's allowed into the ordered reason.  */
  unsigned ordered_cur;

  /* This is the number of threads that have registered themselves in
     the circular queue ordered_team_ids.  */
  unsigned ordered_num_used;

  /* This is the team_id of the currently acknoledged owner of the ordered
     section, or -1u if the ordered section has not been acknowledged by
     any thread.  This is distinguished from the thread that is *allowed*
     to take the section next.  */
  unsigned ordered_owner;

  /* This lock protects the update of the following members.  */
  gomp_mutex_t lock __attribute__((aligned (64)));;

  /* This is the count of the number of threads that have exited the work
     share construct.  If the construct was marked nowait, they have moved on
     to other work; otherwise they're blocked on a barrier.  The last member
     of the team to exit the work share construct must deallocate it.  */
  unsigned threads_completed;

  union {
    /* This is the next iteration value to be allocated.  In the case of
       GFS_STATIC loops, this the iteration start point and never changes.  */
    long next;

    /* This is the returned data structure for SINGLE COPYPRIVATE.  */
    void *copyprivate;
  };

  /* This is a circular queue that details which threads will be allowed
     into the ordered region and in which order.  When a thread allocates
     iterations on which it is going to work, it also registers itself at
     the end of the array.  When a thread reaches the ordered region, it
     checks to see if it is the one at the head of the queue.  If not, it
     blocks on its RELEASE semaphore.  */
  unsigned ordered_team_ids[];
};

/* This structure contains all of the thread-local data associated with 
   a thread team.  This is the data that must be saved when a thread
   encounters a nested PARALLEL construct.  */

struct gomp_team_state
{
  /* This is the team of which the thread is currently a member.  */
  struct gomp_team *team;

  /* This is the work share construct which this thread is currently
     processing.  Recall that with NOWAIT, not all threads may be 
     processing the same construct.  This value is NULL when there
     is no construct being processed.  */
  struct gomp_work_share *work_share;

  /* This is the ID of this thread within the team.  This value is
     guaranteed to be between 0 and N-1, where N is the number of
     threads in the team.  */
  unsigned team_id;

  /* The work share "generation" is a number that increases by one for
     each work share construct encountered in the dynamic flow of the
     program.  It is used to find the control data for the work share
     when encountering it for the first time.  This particular number
     reflects the generation of the work_share member of this struct.  */
  unsigned work_share_generation;

  /* For GFS_RUNTIME loops that resolved to GFS_STATIC, this is the
     trip number through the loop.  So first time a particular loop
     is encountered this number is 0, the second time through the loop
     is 1, etc.  This is unused when the compiler knows in advance that
     the loop is statically scheduled.  */
  unsigned long static_trip;

  /* Number of single stmts encountered.  */
  unsigned long single_count;

};

/* This structure describes a "team" of threads.  These are the threads
   that are spawned by a PARALLEL constructs, as well as the work sharing
   constructs that the team encounters.  */

struct gomp_team
{
  /* This lock protects access to the following work shares data structures.  */
  gomp_mutex_t work_share_lock;

  /* This is a dynamically sized array containing pointers to the control
     structs for all "live" work share constructs.  Here "live" means that
     the construct has been encountered by at least one thread, and not
     completed by all threads.  */
  struct gomp_work_share **work_shares;

  /* The work_shares array is indexed by "generation & generation_mask".
     The mask will be 2**N - 1, where 2**N is the size of the array.  */
  unsigned generation_mask;

  /* These two values define the bounds of the elements of the work_shares
     array that are currently in use.  */
  unsigned oldest_live_gen;
  unsigned num_live_gen;

  /* This is the number of threads in the current team.  */
  unsigned nthreads;

  /* Number of simple single regions encountered by threads in this
     team.  */
  unsigned long single_count;

  /* This is the saved team state that applied to a master thread before
     the current thread was created.  */
  struct gomp_team_state prev_ts;

  /* This barrier is used for most synchronization of the team.  */
  gomp_barrier_t barrier;

  /* This semaphore should be used by the master thread instead of its
     "native" semaphore in the thread structure.  Required for nested
     parallels, as the master is a member of two teams.  */
  gomp_sem_t master_release;

#ifdef LIBGOMP_USE_MARCEL_BUBBLES

  /* The bubble that contains the threads of this team. */
  marcel_bubble_t bubble;

#endif /* LIBGOMP_USE_MARCEL_BUBBLES */

  /* This is the generation of the current team (0 for the master team). */
  unsigned generation;

  struct gomp_thread_start_data *start_data;

  /* Unique string to represent a team (mostly used for debugging purpose). */
  char name[32];

  /* This array contains pointers to the release semaphore of the threads
     in the team.  */
  gomp_sem_t *ordered_release[];
};

/* This structure contains all data that is private to libgomp and is
   allocated per thread.  */

struct gomp_thread
{
  /* This is the function that the thread should run upon launch.  */
  void (*fn) (void *data);
  void *data;

  /* This is the current team state for this thread.  The ts.team member
     is NULL only if the thread is idle.  */
  struct gomp_team_state ts;

  /* This semaphore is used for ordered loops.  */
  gomp_sem_t release;

  /* useful to insert the thread in a bubble */
  marcel_t tid;

  /* The number of threads this thread will create when calling
     gomp_team_start (). */
  int private_num_threads;

#ifdef HAVE_MARCEL_MAMI

  /* List of the memory areas attached to the current thread. */
  p_tbx_slist_t memory_areas;

  /* Boolean variable set to ``true'' when attaching some
     memory pages to the current thread. */
  unsigned int has_attached_memory_pages;

  /* Per-thread variable accessed by the bubble scheduler to decide
     whether threads will be distributed or not on upcoming team
     creation. */
  unsigned int shake_on_team_creation;

#endif
#ifdef LIBGOMP_USE_TASKS
   int untied_task_recursion_level;
#endif
};

#ifdef LIBGOMP_USE_TASKS
struct gomp_task_region
{
  /* Linked list of task regions when nesting */
  struct gomp_task_region *prev_tr;

  /* Bubble holding the set of direct task childs. */
  marcel_bubble_t task_bubble;

  /* Recursive depth */
  int depth;
};
#endif

#ifdef HAVE_MARCEL_MAMI

/* Flush the current thread's memory hints. */
extern int gomp_memory_clean_thread (struct gomp_thread *);

/* Check whether the current thread distribution needs to be
   reviewed. */
extern void gomp_team_shake (struct gomp_team *);

/* Attach the memory areas stored by the main thread of the team. */
extern void gomp_memory_attach_to_thread (struct gomp_thread *, marcel_t *, unsigned int);

#endif 


/* ... and here is that TLS data.  */

extern marcel_key_t gomp_tls_key;
static inline struct gomp_thread *gomp_thread (void)
{
  return marcel_getspecific (gomp_tls_key);
}

/* These are the OpenMP 2.5 internal control variables described in
   section 2.3.  At least those that correspond to environment variables.  */

extern unsigned long gomp_nthreads_var;
extern bool gomp_dyn_var;
extern bool gomp_nest_var;
extern enum gomp_schedule_type gomp_run_sched_var;
extern unsigned long gomp_run_sched_chunk;

/* These are ForestGOMP extensions to OpenMP's internal control
   variables.  */

extern bool fgomp_disable_preemption;


/* The attributes to be used during thread creation.  */
extern marcel_attr_t gomp_thread_attr;

/* Function prototypes.  */

/* alloc.c */

extern void *gomp_malloc (size_t) __attribute__((__malloc__));
extern void *gomp_malloc_cleared (size_t) __attribute__((__malloc__));
extern void *gomp_realloc (void *, size_t);
extern void gomp_free (void *);

/* Avoid conflicting prototypes of alloca() in system headers by using
   GCC's builtin alloca().  */
#define gomp_alloca(x)  __builtin_alloca(x)

/* iter.c */

extern int gomp_iter_static_next (long *, long *);
extern bool gomp_iter_dynamic_next_locked (long *, long *);
extern bool gomp_iter_guided_next_locked (long *, long *);

#ifdef HAVE_SYNC_BUILTINS
extern bool gomp_iter_dynamic_next (long *, long *);
extern bool gomp_iter_guided_next (long *, long *);
#endif

/* ordered.c */

extern void gomp_ordered_first (void);
extern void gomp_ordered_last (void);
extern void gomp_ordered_next (void);
extern void gomp_ordered_static_init (void);
extern void gomp_ordered_static_next (void);
extern void gomp_ordered_sync (void);

/* parallel.c */

extern unsigned gomp_resolve_num_threads (unsigned);

/* proc.c (in config/) */

extern void gomp_init_num_threads (void);
extern unsigned gomp_dynamic_max_threads (void);

/* team.c */

extern void gomp_team_start (void (*) (void *), void *, unsigned,
			     struct gomp_work_share *);
extern void gomp_team_end (void);

/* work.c */

extern struct gomp_work_share * gomp_new_work_share (bool, unsigned);
extern bool gomp_work_share_start (bool);
extern void gomp_work_share_end (void);
extern void gomp_work_share_end_nowait (void);

/* omp task */
#ifdef LIBGOMP_USE_TASKS
extern marcel_key_t gomp_tr_key;
static inline struct gomp_task_region *gomp_tr (void)
{
  return marcel_getspecific (gomp_tr_key);
}

static inline void gomp_init_task_bubble (struct gomp_task_region *tr) {
  marcel_bubble_t *holding_bubble = marcel_bubble_holding_task (marcel_self());
  marcel_bubble_init (&tr->task_bubble);
  marcel_bubble_scheduleonthreadholder (&tr->task_bubble);
  marcel_bubble_setprio (&tr->task_bubble, MA_DEF_PRIO);
  marcel_bubble_insertbubble (holding_bubble, &tr->task_bubble);
}

static inline void gomp_destroy_task_bubble (struct gomp_task_region *tr) {
  marcel_seed_try_inlining (&tr->task_bubble, 0, 0);
  marcel_bubble_join (&tr->task_bubble);
  marcel_bubble_destroy (&tr->task_bubble);
}

static inline void gomp_init_task_region (int depth) {
  struct gomp_task_region *prev_tr = gomp_tr ();
  struct gomp_task_region *tr = gomp_malloc (sizeof (struct gomp_task_region));
  gomp_init_task_bubble (tr);
  tr->prev_tr = prev_tr;
  tr->depth = depth;
  marcel_setspecific (gomp_tr_key, tr);
}

static inline void gomp_destroy_task_region (void) {
  struct gomp_task_region *tr = gomp_tr ();
  struct gomp_task_region *prev_tr;
  assert (tr != NULL);
  gomp_destroy_task_bubble (tr);
  prev_tr = tr->prev_tr;
  gomp_free (tr);
  marcel_setspecific (gomp_tr_key, prev_tr);
}

static inline void gomp_team_process_uninstantiated_tasks (void) {
  struct gomp_thread * const thr = gomp_thread ();
  if (!thr)
	  return;

  struct gomp_team * const team = thr->ts.team;
  if (!team)
	  return;

  const bool nested = team->prev_ts.team != NULL;

  if (team->nthreads > 1 && nested)
  {
    marcel_seed_try_inlining (&team->bubble, 1, 1);
  }
  else
  {
    marcel_seed_try_inlining (&marcel_root_bubble, 1, 1);
  }
}
#else
#define gomp_init_task_bubble(tr)
#define gomp_destroy_task_bubble(tr)
#define gomp_init_task_region(depth)
#define gomp_destroy_task_region()
#define gomp_team_process_uninstantiated_tasks()
#endif
void gomp_task_display_stats (void);

#ifdef HAVE_ATTRIBUTE_VISIBILITY
# pragma GCC visibility pop
#endif

/* Now that we're back to default visibility, include the globals.  */
#include "libgomp_g.h"

/* Include omp.h by parts.  */
#include "omp-lock.h"
#define _LIBGOMP_OMP_LOCK_DEFINED 1
#include "omp.h.in"

#ifdef HAVE_ATTRIBUTE_VISIBILITY
# define attribute_hidden __attribute__ ((visibility ("hidden")))
#else
# define attribute_hidden
#endif

#ifdef HAVE_ATTRIBUTE_ALIAS
# define ialias(fn) \
  extern __typeof (fn) gomp_ialias_##fn \
    __attribute__ ((alias (#fn))) attribute_hidden;
#else
# define ialias(fn)
#endif

#endif /* LIBGOMP_H */
