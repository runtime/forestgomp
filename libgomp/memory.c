/* Copyright (C) 2008 Free Software Foundation, Inc.

   This file is part of the GNU OpenMP Library (libgomp).

   Libgomp is free software; you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 2.1 of the License, or
   (at your option) any later version.

   Libgomp is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with libgomp; see the file COPYING.LIB.  If not, write to the
   Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.  */

/* As a special exception, if you link this library with other files, some
   of which are compiled with GCC, to produce an executable, this library
   does not by itself cause the resulting executable to be covered by the
   GNU General Public License.  This exception does not however invalidate
   any other reasons why the executable file might be covered by the GNU
   General Public License.  */

#include "libgomp.h"
#include <forestgomp.h>

#include <stdarg.h>

void *
fgomp_malloc (size_t buffer_size)
{
  return mami_malloc (fgomp_memory_manager, buffer_size, MAMI_MEMBIND_POLICY_FIRST_TOUCH, 0);
}

void *
fgomp_malloc_on_node (size_t buffer_size, unsigned int node)
{
  return mami_malloc (fgomp_memory_manager, buffer_size, MAMI_MEMBIND_POLICY_SPECIFIC_NODE, node);
}

int
fgomp_migrate_on_next_touch (void *buffer, size_t size)
{
  mami_register (fgomp_memory_manager, buffer, size);
  return mami_migrate_on_next_touch (fgomp_memory_manager, buffer);
}

int
fgomp_attach_on_next_touch (void *buffer, size_t size)
{
  mami_register (fgomp_memory_manager, buffer, size);
  return mami_attach_on_next_touch (fgomp_memory_manager, buffer);
}

void
fgomp_free (void *buffer)
{
  mami_free (fgomp_memory_manager, buffer);
}

int
fgomp_set_next_team_affinity (void *buffer, 
			      size_t chunk_size, 
			      unsigned int mode)
{
  struct gomp_thread *self = gomp_thread ();

  /* Inform the underlying bubble scheduler we added affinity
     relations on the borning team. */
  self->shake_on_team_creation = mode;

  /* Attach chunk 0 to thread 0. */
  int node;
  mami_task_attach (fgomp_memory_manager, buffer, chunk_size, self->tid, &node);
  
  /* Save the memory information inside the main thread. */
  struct fgomp_memory_area *fgomp_ma = gomp_malloc (sizeof (struct fgomp_memory_area));
  fgomp_ma->buffer = buffer;
  fgomp_ma->chunk_size = chunk_size;
  tbx_slist_push (self->memory_areas, fgomp_ma);

  self->has_attached_memory_pages = 1;

  /* Inform the caller whether we managed to attach memory or not. */
  return node;
}

int
fgomp_set_current_thread_affinity (void *buffer, size_t size, fgomp_shake_mode_t mode)
{
  int node, attach;
  struct gomp_thread *self = gomp_thread ();

  self->has_attached_memory_pages = 1;
  attach = mami_task_attach (fgomp_memory_manager, buffer, size, self->tid, &node);

  if (mode == FGOMP_SHAKE_WAIT)
    {
      /* Synchronize the threads and let the main thread shake the
	 distribution. */
      gomp_barrier_wait (&self->ts.team->barrier, self->ts.team_id);
      if (self->ts.team_id == 0)
	marcel_bubble_shake ();
    }
  else
    /* Let every thread of the team shake the distribution. */
    marcel_bubble_shake ();

  return attach;
}

void
gomp_memory_attach_to_thread (struct gomp_thread *source, marcel_t *target, unsigned int target_index)
{
  /* TODO: Hackish way to deal with static scheduling
     policy. Extend this approach to any scheduling policy by
     updating this information at work share time. */
  if (!tbx_slist_is_nil (source->memory_areas))
    {
      tbx_slist_ref_to_head (source->memory_areas);
      do {
	int node;
	struct fgomp_memory_area *ma = tbx_slist_ref_get (source->memory_areas);
	unsigned int my_offset = target_index * ma->chunk_size;
	mami_task_attach (fgomp_memory_manager, (char *)ma->buffer + my_offset, ma->chunk_size, *target, &node);
	if (node < 0)
	  marcel_fprintf (stderr, "Warning: MaMI didn't manage to update the memnode statistics!\n");
      } while (tbx_slist_ref_forward (source->memory_areas));
    } 
}

void
gomp_team_shake (struct gomp_team *team) 
{
  struct gomp_thread *thr = gomp_thread ();

  /* Synchronized shake: wait until every thread of the upper team
     reaches this point before calling shake () only once. */
  if (thr->shake_on_team_creation == FGOMP_SHAKE_WAIT)
    {
      struct gomp_team_state *upper_team_state = &team->prev_ts;
      if (upper_team_state->team)
	{
	  gomp_barrier_wait (&upper_team_state->team->barrier, upper_team_state->team_id);
	  thr->shake_on_team_creation = FGOMP_DONT_SHAKE;
	}
      
      if (upper_team_state->team_id == 0)
	marcel_bubble_shake ();
    }
  /* Immediate shake: call shake () every time a thread reaches
     this point. */
  else if (thr->shake_on_team_creation == FGOMP_SHAKE_NOWAIT)
    {
      marcel_bubble_shake ();
      thr->shake_on_team_creation = FGOMP_DONT_SHAKE;
    }
}

int
gomp_memory_clean_thread (struct gomp_thread *thr)
{
  if (thr->has_attached_memory_pages)
    {
      mami_task_unattach_all (fgomp_memory_manager, thr->tid);

      while (!tbx_slist_is_nil (thr->memory_areas))
	{
	  struct fgomp_memory_area *ma = tbx_slist_pop (thr->memory_areas);

	  if (ma != NULL)
	    gomp_free (ma);
	}

      thr->has_attached_memory_pages = 0;
    }

  return 0;
}

/* Fortran functions. */

void * 
fgomp_malloc_ (unsigned int *buffer_size) 
{
  return fgomp_malloc (*buffer_size);
}

void *
fgomp_malloc_on_node_ (unsigned int *buffer_size, unsigned int *node) 
{
  return fgomp_malloc_on_node (*buffer_size, *node);
}

int 
fgomp_migrate_on_next_touch_ (void *buffer, unsigned int *size) 
{
  return fgomp_migrate_on_next_touch (buffer, *size);
}

int
fgomp_set_next_team_affinity_ (void *buffer, 
			       unsigned int *chunk_size, 
			       unsigned int *mode)
{
  return fgomp_set_next_team_affinity (buffer, *chunk_size, *mode);
}

int 
fgomp_set_current_thread_affinity_ (void *buffer, unsigned int *size, unsigned int *mode)
{
  return fgomp_set_current_thread_affinity (buffer, *size, *mode);
}

void
fgomp_free_ (void *buffer)
{
  fgomp_free (buffer);
}
