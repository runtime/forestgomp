/* slab.h - The GNU Hurd slab allocator interface.
   Copyright (C) 2003, 2005 Free Software Foundation, Inc.
   Written by Marcus Brinkmann <marcus@gnu.org>
   Slightly modified for libgomp by Ludovic Courtès <ludo@gnu.org>

   This file is part of the GNU Hurd.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this program; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */

#ifndef _GOMP_SLAB_H
#define _GOMP_SLAB_H	1

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <errno.h>
#include <stdbool.h>
#include <marcel.h>

/* If GNU's `error_t' isn't defined, define it.  */
#ifndef HAVE_ERROR_T
typedef int error_t;
#endif


/* Allocate a buffer in *PTR of size SIZE which must be a power of 2
   and self aligned (i.e. aligned on a SIZE byte boundary).  HOOK is
   as provided to gomp_slab_create.  Return 0 on success, an error
   code on failure.  */
typedef error_t (*gomp_slab_allocate_buffer_t) (void *hook, size_t size,
						void **ptr);

/* Deallocate buffer BUFFER of size SIZE.  HOOK is as provided to
   gomp_slab_create.  */
typedef error_t (*gomp_slab_deallocate_buffer_t) (void *hook, void *buffer,
						  size_t size);

/* Initialize the slab object pointed to by OBJECT.  HOOK is as
   provided to gomp_slab_create.  */
typedef error_t (*gomp_slab_constructor_t) (void *hook, void *object);

/* Destroy the slab object pointed to by OBJECT.  HOOK is as provided
   to gomp_slab_create.  */
typedef void (*gomp_slab_destructor_t) (void *hook, void *object);


/* The type of a slab space.  

   The structure is divided into two parts: the first is only used
   while the slab space is constructed.  Its fields are either
   initialized by a static initializer (GOMP_SLAB_SPACE_INITIALIZER)
   or by the gomp_slab_create function.  The initialization of the
   space is delayed until the first allocation.  After that only the
   second part is used.  */

typedef struct gomp_slab_space *gomp_slab_space_t;
struct gomp_slab_space
{
  /* First part.  Used when initializing slab space object.   */
  
  /* True if slab space has been initialized.  */
  bool initialized;
  
  /* Protects this structure, along with all the slabs.  No need to
     delay initialization of this field.  */
  marcel_spinlock_t lock;

  /* The size and alignment of objects allocated using this slab
     space.  These to fields are used to calculate the final object
     size, which is put in SIZE (defined below).  */
  size_t requested_size;
  size_t requested_align;

  /* The buffer allocator.  */
  gomp_slab_allocate_buffer_t allocate_buffer;

  /* The buffer deallocator.  */
  gomp_slab_deallocate_buffer_t deallocate_buffer;

  /* The constructor.  */
  gomp_slab_constructor_t constructor;

  /* The destructor.  */
  gomp_slab_destructor_t destructor;

  /* The user's private data.  */
  void *hook;

  /* Second part.  Runtime information for the slab space.  */

  struct gomp_slab *slab_first;
  struct gomp_slab *slab_last;

  /* In the doubly-linked list of slabs, empty slabs come first, after
     that the slabs that have some buffers allocated, and finally the
     complete slabs (refcount == 0).  FIRST_FREE points to the first
     non-empty slab.  */
  struct gomp_slab *first_free;

  /* For easy checking, this holds the value the reference counter
     should have for an empty slab.  */
  int full_refcount;

  /* The size of one object.  Should include possible alignment as
     well as the size of the bufctl structure.  */
  size_t size;
};


/* Static initializer.  TYPE is used to get size and alignment of
   objects the slab space will be used to allocate.  ALLOCATE_BUFFER
   may be NULL in which case mmap is called.  DEALLOCATE_BUFFER may be
   NULL in which case munmap is called.  CTOR and DTOR are the slab's
   object constructor and destructor, respectivly and may be NULL if
   not required.  HOOK is passed as user data to the constructor and
   destructor.  */
#define GOMP_SLAB_SPACE_INITIALIZER(TYPE, ALLOC, DEALLOC, CTOR,	\
				    DTOR, HOOK)			\
  {								\
    false,							\
    MARCEL_SPINLOCK_INITIALIZER,				\
    sizeof (TYPE),						\
    __alignof__ (TYPE),						\
    ALLOC,							\
    DEALLOC,							\
    CTOR,							\
    DTOR,							\
    HOOK							\
    /* The rest of the structure will be filled with zeros,     \
       which is good for us.  */				\
  }


/* Create a new slab space with the given object size, alignment,
   constructor and destructor.  ALIGNMENT can be zero.
   ALLOCATE_BUFFER may be NULL in which case mmap is called.
   DEALLOCATE_BUFFER may be NULL in which case munmap is called.  CTOR
   and DTOR are the slabs object constructor and destructor,
   respectivly and may be NULL if not required.  HOOK is passed as the
   first argument to the constructor and destructor.  */
error_t gomp_slab_create (size_t size, size_t alignment,
			  gomp_slab_allocate_buffer_t allocate_buffer,
			  gomp_slab_deallocate_buffer_t deallocate_buffer,
			  gomp_slab_constructor_t constructor,
			  gomp_slab_destructor_t destructor,
			  void *hook,
			  gomp_slab_space_t *space);

/* Destroy all objects and the slab space SPACE.  If there were no
   outstanding allocations free the slab space.  Returns EBUSY if
   there are still allocated objects in the slab space.  The dual of
   gomp_slab_create.  */
error_t gomp_slab_free (gomp_slab_space_t space);

/* Like gomp_slab_create, but does not allocate storage for the slab.  */
error_t gomp_slab_init (gomp_slab_space_t space, size_t size, size_t alignment,
			gomp_slab_allocate_buffer_t allocate_buffer,
			gomp_slab_deallocate_buffer_t deallocate_buffer,
			gomp_slab_constructor_t constructor,
			gomp_slab_destructor_t destructor,
			void *hook);

/* Destroy all objects and the slab space SPACE.  Returns EBUSY if
   there are still allocated objects in the slab.  The dual of
   gomp_slab_init.  */
error_t gomp_slab_destroy (gomp_slab_space_t space);

/* Allocate a new object from the slab space SPACE.  */
error_t gomp_slab_alloc (gomp_slab_space_t space, void **buffer);

/* Deallocate the object BUFFER from the slab space SPACE.  */
void gomp_slab_dealloc (gomp_slab_space_t space, void *buffer);

/* Create a more strongly typed slab interface a la a C++ template.

   NAME is the name of the new slab class.  NAME is used to synthesize
   names for the class types and methods using the following rule: the
   gomp_ namespace will prefix all method names followed by NAME
   followed by an underscore and finally the method name.  The
   following are thus exposed:

    Types:
     struct gomp_NAME_slab_space
     gomp_NAME_slab_space_t

     error_t (*gomp_NAME_slab_constructor_t) (void *hook, element_type *buffer)
     void (*gomp_NAME_slab_destructor_t) (void *hook, element_type *buffer)

    Functions:
     error_t gomp_NAME_slab_create (gomp_slab_allocate_buffer_t
                                     allocate_buffer,
                                    gomp_slab_deallocate_buffer_t
                                     deallocate_buffer,
                                    gomp_NAME_slab_constructor_t constructor,
                                    gomp_NAME_slab_destructor_t destructor,
                                    void *hook,
                                    gomp_NAME_slab_space_t *space);
     error_t gomp_NAME_slab_free (gomp_NAME_slab_space_t space);

     error_t gomp_NAME_slab_init (gomp_NAME_slab_space_t space,
                                  gomp_slab_allocate_buffer_t allocate_buffer,
                                  gomp_slab_deallocate_buffer_t
                                   deallocate_buffer,
                                  gomp_NAME_slab_constructor_t constructor,
                                  gomp_NAME_slab_destructor_t destructor,
                                  void *hook);
     error_t gomp_NAME_slab_destroy (gomp_NAME_slab_space_t space);

     error_t gomp_NAME_slab_alloc (gomp_NAME_slab_space_t space,
                                   element_type **buffer);
     void gomp_NAME_slab_dealloc (gomp_NAME_slab_space_t space,
                                  element_type *buffer);

  ELEMENT_TYPE is the type of elements to store in the slab.  If you
  want the slab to contain struct foo, pass `struct foo' as the
  ELEMENT_TYPE (not `struct foo *'!!!).
     
*/
#define SLAB_CLASS(name, element_type)                                       \
struct gomp_##name##_slab_space						     \
{									     \
  struct gomp_slab_space space;						     \
};									     \
typedef struct gomp_##name##_slab_space *gomp_##name##_slab_space_t;	     \
									     \
typedef error_t (*gomp_##name##_slab_constructor_t) (void *hook,	     \
						     element_type *buffer);  \
									     \
typedef void (*gomp_##name##_slab_destructor_t) (void *hook,		     \
						 element_type *buffer);	     \
									     \
static inline error_t							     \
gomp_##name##_slab_create (gomp_slab_allocate_buffer_t allocate_buffer,	     \
			   gomp_slab_deallocate_buffer_t deallocate_buffer,  \
			   gomp_##name##_slab_constructor_t constructor,     \
			   gomp_##name##_slab_destructor_t destructor,	     \
			   void *hook,					     \
			   gomp_##name##_slab_space_t *space)		     \
{									     \
  union									     \
  {									     \
    gomp_##name##_slab_constructor_t t;					     \
    gomp_slab_constructor_t u;						     \
  } con;								     \
  union									     \
  {									     \
    gomp_##name##_slab_destructor_t t;					     \
    gomp_slab_destructor_t u;						     \
  } des;								     \
  union									     \
  {									     \
    gomp_##name##_slab_space_t *t;					     \
    gomp_slab_space_t *u;						     \
  } foo;								     \
  con.t = constructor;							     \
  des.t = destructor;							     \
  foo.t = space;							     \
									     \
  return gomp_slab_create(sizeof (element_type), __alignof__ (element_type), \
			  allocate_buffer, deallocate_buffer,		     \
			  con.u, des.u, hook, foo.u);			     \
}									     \
									     \
static inline error_t							     \
gomp_##name##_slab_free (gomp_##name##_slab_space_t space)		     \
{									     \
  return gomp_slab_free (&space->space);				     \
}									     \
									     \
static inline error_t							     \
gomp_##name##_slab_init (gomp_##name##_slab_space_t space,		     \
			 gomp_slab_allocate_buffer_t allocate_buffer,	     \
			 gomp_slab_deallocate_buffer_t deallocate_buffer,    \
			 gomp_##name##_slab_constructor_t constructor,	     \
			 gomp_##name##_slab_destructor_t destructor,	     \
			 void *hook)					     \
{									     \
  union									     \
  {									     \
    gomp_##name##_slab_constructor_t t;					     \
    gomp_slab_constructor_t u;						     \
  } con;								     \
  union									     \
  {									     \
    gomp_##name##_slab_destructor_t t;					     \
    gomp_slab_destructor_t u;						     \
  } des;								     \
  con.t = constructor;							     \
  des.t = destructor;							     \
									     \
  return gomp_slab_init (&space->space,					     \
			 sizeof (element_type), __alignof__ (element_type),  \
			 allocate_buffer, deallocate_buffer,		     \
			 con.u, des.u, hook);				     \
}									     \
									     \
static inline error_t							     \
gomp_##name##_slab_destroy (gomp_##name##_slab_space_t space)		     \
{									     \
  return gomp_slab_destroy (&space->space);				     \
}									     \
									     \
static inline error_t							     \
gomp_##name##_slab_alloc (gomp_##name##_slab_space_t space,		     \
			  element_type **buffer)			     \
{									     \
  union									     \
  {									     \
    element_type **e;							     \
    void **v;								     \
  } foo;								     \
  foo.e = buffer;							     \
									     \
  return gomp_slab_alloc (&space->space, foo.v);			     \
}									     \
									     \
static inline void							     \
gomp_##name##_slab_dealloc (gomp_##name##_slab_space_t space,		     \
			    element_type *buffer)			     \
{									     \
  union									     \
  {									     \
    element_type *e;							     \
    void *v;								     \
  } foo;								     \
  foo.e = buffer;							     \
									     \
  gomp_slab_dealloc (&space->space, foo.v);				     \
}

#endif	/* _GOMP_SLAB_H */
