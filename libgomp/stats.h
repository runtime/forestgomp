/* Copyright (C) 2009 Free Software Foundation, Inc.
   Contributed by Richard Henderson <rth@redhat.com>.

   This file is part of the GNU OpenMP Library (libgomp).

   Libgomp is free software; you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 2.1 of the License, or
   (at your option) any later version.

   Libgomp is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with libgomp; see the file COPYING.LIB.  If not, write to the
   Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.  */

/* As a special exception, if you link this library with other files, some
   of which are compiled with GCC, to produce an executable, this library
   does not by itself cause the resulting executable to be covered by the
   GNU General Public License.  This exception does not however invalidate
   any other reasons why the executable file might be covered by the GNU
   General Public License.  */

/* This file contains data types and function declarations of the
   ForestGOMP statistics module.  */

/* Keywords you want to keep an eye on. */
enum
  {
    FGOMP_STATS_ATOMIC,
    FGOMP_STATS_BARRIER,
    FGOMP_STATS_CRITICAL,
    FGOMP_STATS_ORDERED,
    FGOMP_STATS_PARALLEL,
    FGOMP_STATS_LOOP,
    FGOMP_STATS_PARALLEL_LOOP,
    FGOMP_STATS_LOOP_STATIC,
    FGOMP_STATS_LOOP_DYNAMIC,
    FGOMP_STATS_LOOP_GUIDED
  };

typedef struct fgomp_statistics fgomp_statistics_t;

struct fgomp_statistics
{
  unsigned int nb_atomic;
  unsigned int nb_barrier;
  unsigned int nb_critical;
  unsigned int nb_ordered;
  unsigned int nb_parallel;
  unsigned int nb_loop;
  unsigned int nb_parallel_loop;
  unsigned int nb_loop_static;
  unsigned int nb_loop_dynamic;
  unsigned int nb_loop_guided;
};

#if defined FGOMP_STATS
/* Increment the statistics for the considered keyword. */
# define write_statistics(stats, keyword)	\
  do						\
    {						\
      switch (keyword)				\
	{					\
	case FGOMP_STATS_ATOMIC:		\
	  if (gomp_thread ()->ts.team_id == 0)	\
	    (stats)->nb_atomic++;		\
	  break;				\
	case FGOMP_STATS_BARRIER:		\
	  if (gomp_thread ()->ts.team_id == 0)	\
	    (stats)->nb_barrier++;		\
	  break;				\
	case FGOMP_STATS_CRITICAL:		\
	  if (gomp_thread ()->ts.team_id == 0)	\
	    (stats)->nb_critical++;		\
	  break;				\
	case FGOMP_STATS_ORDERED:		\
	  if (gomp_thread ()->ts.team_id == 0)	\
	    (stats)->nb_ordered++;		\
	  break;				\
	case FGOMP_STATS_PARALLEL:		\
	  (stats)->nb_parallel++;		\
	  break;				\
	case FGOMP_STATS_LOOP:			\
	  (stats)->nb_loop++;			\
	  break;				\
	case FGOMP_STATS_PARALLEL_LOOP:	       	\
	  (stats)->nb_parallel_loop++;		\
	  break;				\
	case FGOMP_STATS_LOOP_STATIC:  		\
	  (stats)->nb_loop_static++;   		\
	  break;				\
	case FGOMP_STATS_LOOP_DYNAMIC: 		\
	  (stats)->nb_loop_dynamic++;	        \
	  break;				\
	case FGOMP_STATS_LOOP_GUIDED:  		\
	  (stats)->nb_loop_guided++;	       	\
	  break;				\
						\
	default:				\
	  MA_BUG ();				\
	}					\
    }						\
  while (0)

/* Print the gathered statistics. */
# define print_statistics(stats)					\
  do									\
    {									\
      marcel_printf ("\nPrinting ForestGOMP statistics:\n");		\
      marcel_printf ("# atomic:         %10u\n", (stats)->nb_atomic);	\
      marcel_printf ("# barrier:        %10u\n", (stats)->nb_barrier);	\
      marcel_printf ("# critical:       %10u\n", (stats)->nb_critical);	\
      marcel_printf ("# ordered:        %10u\n", (stats)->nb_ordered);	\
      marcel_printf ("# parallel:       %10u\n", (stats)->nb_parallel);	\
      marcel_printf ("# loop:           %10u\n", (stats)->nb_loop);    	\
      marcel_printf ("#   |->  static:    %10u\n", (stats)->nb_loop_static); \
      marcel_printf ("#   |->  dynamic:   %10u\n", (stats)->nb_loop_dynamic); \
      marcel_printf ("#   |->  guided:    %10u\n", (stats)->nb_loop_guided); \
      marcel_printf ("# parallel loop:  %10u\n", (stats)->nb_parallel_loop); \
    }									\
  while (0)
#else  /* !FGOMP_STATS */
# define write_statistics(stat, keyword) (void)0
# define print_statistics(stat) (void)0
#endif /* !FGOMP_STATS */
