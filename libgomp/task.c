/* Copyright (C) 2005 Free Software Foundation, Inc.
   Contributed by Richard Henderson <rth@redhat.com>.

   This file is part of the GNU OpenMP Library (libgomp).

   Libgomp is free software; you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 2.1 of the License, or
   (at your option) any later version.

   Libgomp is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License 
   along with libgomp; see the file COPYING.LIB.  If not, write to the
   Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.  */

/* As a special exception, if you link this library with other files, some
   of which are compiled with GCC, to produce an executable, this library
   does not by itself cause the resulting executable to be covered by the
   GNU General Public License.  This exception does not however invalidate
   any other reasons why the executable file might be covered by the GNU
   General Public License.  */

/* This file handles the (bare) TASK construct.  */

#define MARCEL_INTERNAL_INCLUDE
#include "libgomp.h"

#ifdef LIBGOMP_USE_TASKS

/* Stats */
static ma_atomic_t gomp_task_stat_tied_task_count;
static ma_atomic_t gomp_task_stat_untied_task_count;
static ma_atomic_t gomp_task_stat_cpyfn_called;
static ma_atomic_t gomp_task_stat_memcpy_called;
static ma_atomic_t gomp_task_stat_pending_untied_tasks;
static ma_atomic_t gomp_task_stat_max_pending_untied_tasks;
static ma_atomic_t gomp_task_stat_max_untied_tasks_recursion_level;

/* This structure is used to communicate across marcel_create. */
struct gomp_task_thread_start_data
{
  struct gomp_team_state ts;
  void (*fn) (void *);
  void *fn_data;
  int depth;
};

static void *
gomp_task_thread_start (void *xdata)
{
  struct gomp_task_thread_start_data *task_data = xdata;
  struct gomp_thread local_thr;
  struct gomp_thread * const thr = &local_thr;
  struct gomp_thread * const backup_thr = marcel_getspecific (gomp_tls_key);

  {
    int v = (backup_thr)?backup_thr->untied_task_recursion_level+1:0;
    int m = ma_atomic_read (&gomp_task_stat_max_untied_tasks_recursion_level);
    while (v > m) {
      int o = ma_atomic_xchg (m, v, &gomp_task_stat_max_untied_tasks_recursion_level);
      if (o == m) {
        break;
      }
      m = o;
    }

    thr->untied_task_recursion_level = v;
  }

  marcel_setspecific (gomp_tls_key, thr);
  if (backup_thr) {
    thr->ts = backup_thr->ts;
  } else {
    thr->ts = task_data->ts;
  }

  gomp_init_task_region (task_data->depth);

  FGOMP_EVENT_GOMP_TASK_ENTER ((gomp_tr ()->depth));
  task_data->fn (task_data->fn_data);
  FGOMP_EVENT_GOMP_TASK_LEAVE ();

  gomp_destroy_task_region ();
  gomp_free (task_data);
  marcel_setspecific (gomp_tls_key, backup_thr);

  ma_atomic_dec (&gomp_task_stat_pending_untied_tasks);
  return NULL;
}

void
GOMP_task (void (*fn) (void *), void *data, void (*cpyfn) (void *, void *),
    long arg_size, long arg_align, bool if_clause,
    unsigned flags __attribute__((unused)))
{
  /* display task settings */
#if 0
  {
    struct gomp_thread *thr = gomp_thread ();
    struct gomp_task_region *tr = gomp_tr ();
    marcel_printf ("GOMP_task: fn = %p, data = %p, cpyfn = %p, arg_size = %ld, arg_align = %ld, if_clause = %d, flags = %x (%s) --- FGomp: thread = %p, team = %p, task bubble = %p\n",
        fn, data, cpyfn, arg_size, arg_align, (int)if_clause, flags, flags&1?"untied":"tied  ", thr, thr->ts.team, &tr->task_bubble);
  }
#endif

  /* launch task */
  if (flags & 1) {
    struct gomp_thread *thr = gomp_thread ();
    struct gomp_task_region *tr = gomp_tr ();
    struct gomp_task_thread_start_data *task_data = NULL;
    marcel_t t;
    marcel_attr_t gomp_thread_attr;

    ma_atomic_inc (&gomp_task_stat_untied_task_count);
    {
      int v = ma_atomic_inc_return (&gomp_task_stat_pending_untied_tasks);
      int m = ma_atomic_read (&gomp_task_stat_max_pending_untied_tasks);
      while (v > m) {
        int o = ma_atomic_xchg (m, v, &gomp_task_stat_max_pending_untied_tasks);
        if (o == m) {
          if (v % 10 == 0)
          break;
        }
        m = o;
      }
    }

    task_data = gomp_malloc (sizeof (struct gomp_task_thread_start_data) + arg_size+arg_align - 1);
    task_data->ts = thr->ts;
    task_data->fn = fn;
    {
      void *_fn_data = (void *)task_data + sizeof (struct gomp_task_thread_start_data);
      task_data->fn_data = (void *) (((uintptr_t) _fn_data + arg_align - 1) & ~(uintptr_t)(arg_align - 1));
    }
    task_data->depth = tr->depth+1;
    if (cpyfn) {
      ma_atomic_inc (&gomp_task_stat_cpyfn_called);
      cpyfn (task_data->fn_data, data);
    } else {
      ma_atomic_inc (&gomp_task_stat_memcpy_called);
      memcpy (task_data->fn_data, data, arg_size);
    }

    marcel_attr_init (&gomp_thread_attr);
    marcel_attr_setdetachstate (&gomp_thread_attr, tbx_true);
    marcel_attr_setseed (&gomp_thread_attr, tbx_true);
    marcel_attr_setpreemptible (&gomp_thread_attr, !fgomp_disable_preemption);
    marcel_attr_setprio (&gomp_thread_attr, MA_DEF_PRIO);
    marcel_attr_setnaturalbubble (&gomp_thread_attr, &tr->task_bubble);
    marcel_create (&t, &gomp_thread_attr, gomp_task_thread_start, task_data);
  } else {
    char fn_data[(arg_size+arg_align - 1)];
    void *fn_aligned_data = (void *) (((uintptr_t) fn_data + arg_align - 1) & ~(uintptr_t)(arg_align - 1));
    if (cpyfn) {
      ma_atomic_inc (&gomp_task_stat_cpyfn_called);
      cpyfn (fn_aligned_data, data);
    } else {
      ma_atomic_inc (&gomp_task_stat_memcpy_called);
      memcpy (fn_aligned_data, data, arg_size);
    }
    ma_atomic_inc (&gomp_task_stat_tied_task_count);
    fn (fn_aligned_data);
  }
}

void
GOMP_taskwait (void)
{
  struct gomp_task_region *tr = gomp_tr ();
  FGOMP_EVENT_GOMP_TASK_WAIT_ENTER(tr->depth);
  gomp_destroy_task_bubble (tr);
  gomp_init_task_bubble (tr);
  FGOMP_EVENT_GOMP_TASK_WAIT_LEAVE();
}

void
gomp_task_display_stats (void) {
  marcel_printf ("nb tied tasks: \t%lu\n", (unsigned long) ma_atomic_read(&gomp_task_stat_tied_task_count));
  marcel_printf ("nb untied tasks: \t%lu\n", (unsigned long) ma_atomic_read(&gomp_task_stat_untied_task_count));
  marcel_printf ("nb calls to cpyfn to copy task data: \t%lu\n", (unsigned long) ma_atomic_read(&gomp_task_stat_cpyfn_called));
  marcel_printf ("nb calls to memcpy to copy task data: \t%lu\n", (unsigned long)  ma_atomic_read(&gomp_task_stat_memcpy_called));
  marcel_printf ("nb remaining pending untied tasks (should be 0): \t%lu\n", (unsigned long) ma_atomic_read(&gomp_task_stat_pending_untied_tasks));
  marcel_printf ("nb max pending untied tasks: \t%lu\n", (unsigned long) ma_atomic_read(&gomp_task_stat_max_pending_untied_tasks));
  marcel_printf ("nb max untied tasks recursion level: \t%lu\n", (unsigned long) ma_atomic_read(&gomp_task_stat_max_untied_tasks_recursion_level));
}
#else
void
GOMP_task (void (*fn) (void *), void *data, void (*cpyfn) (void *, void *),
    long arg_size, long arg_align, bool if_clause,
    unsigned flags __attribute__((unused)))
{
  marcel_printf("GOMP_task: tasking support not enabled\n");
}

void
GOMP_taskwait (void)
{
  marcel_printf("GOMP_taskwait: tasking support not enabled\n");
}

void
gomp_task_display_stats (void)
{}
#endif

