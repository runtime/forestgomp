/* Copyright (C) 2005 Free Software Foundation, Inc.
   Contributed by Richard Henderson <rth@redhat.com>.

   This file is part of the GNU OpenMP Library (libgomp).

   Libgomp is free software; you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 2.1 of the License, or
   (at your option) any later version.

   Libgomp is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with libgomp; see the file COPYING.LIB.  If not, write to the
   Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.  */

/* As a special exception, if you link this library with other files, some
   of which are compiled with GCC, to produce an executable, this library
   does not by itself cause the resulting executable to be covered by the
   GNU General Public License.  This exception does not however invalidate
   any other reasons why the executable file might be covered by the GNU
   General Public License.  */

/* This file handles the maintainence of threads in response to team
   creation and termination.  */
#define MARCEL_INTERNAL_INCLUDE
#include "libgomp.h"

#include <forestgomp.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <slab.h>
#include <assert.h>

#ifdef HAVE_PUKABI

/* PukABI's public header overwrite `PACKAGE_NAME' et al. */
# undef PACKAGE_NAME
# undef PACKAGE_BUGREPORT
# undef PACKAGE_STRING
# undef PACKAGE_TARNAME
# undef PACKAGE_VERSION
# undef HAVE_ATTRIBUTE_VISIBILITY
# include <Padico/Puk-ABI.h>

#endif

/* This array manages threads spawned from the top level, which will
   return to the idle loop once the current PARALLEL construct ends.  */
static struct gomp_thread **gomp_threads;
static unsigned int gomp_threads_size;
static unsigned int gomp_threads_used;
static ma_atomic_t fgomp_running_teams;

/* This attribute contains MARCEL_CREATE_DETACHED.  */
marcel_attr_t gomp_thread_attr;

/* This barrier holds and releases threads waiting in gomp_threads.  */
static gomp_barrier_t gomp_threads_dock;

/* This is the libgomp per-thread data structure.  */
marcel_key_t gomp_tls_key;
marcel_key_t gomp_tr_key;
static ma_atomic_t cpt;

/* Slab space for `struct gomp_team' objects, indexed by the number of
   threads in the team (since `gomp_team' contains a C99 flexible
   array member whose size depends on the number of threads in the
   team).  */
static gomp_slab_space_t *team_slab_spaces;

/* This structure is used to communicate across marcel_create. */
struct gomp_thread_start_data
{
  struct gomp_team_state ts;
  void (*fn) (void *);
  void *fn_data;
  bool nested;
#ifdef HAVE_MARCEL_MAMI
  unsigned int has_attached_memory_pages;
#endif
#ifdef LIBGOMP_USE_TASKS
  int depth;
#endif
};

#ifdef FGOMP_STATS
/* This structure is used to summarize ForestGOMP statistics. */
fgomp_statistics_t fgomp_stats;
#endif

#ifdef HAVE_MARCEL_MAMI
/* This structure is used to keep track of memory allocations. */
mami_manager_t *fgomp_memory_manager;
#endif

#ifdef PROFILE

/* Colors to be printed by the "bubbles" profiling tool. */
#define FGOMP_NB_COLORS 4

typedef struct fgomp_color 
{
  unsigned char r;
  unsigned char g;
  unsigned char b;
} fgomp_color_t;

fgomp_color_t fgomp_colors[FGOMP_NB_COLORS] = {
  /* Red */
  {.r = 255, .g = 0, .b = 0},
  /* Blue */
  {.r = 0, .g = 0, .b = 255},
  /* Yellow */
  {.r = 255, .g = 255, .b = 0},
  /* Magenta */
  {.r = 255, .g = 0, .b = 255}
};

#endif

/* This function is a marcel_create entry point.  This contains the idle
   loop in which a thread waits to be called up to become part of a team.  */

static void *
gomp_thread_start (void *xdata)
{
  struct gomp_thread_start_data *data = xdata;
  struct gomp_thread *thr;
  void (*local_fn) (void *);
  void *local_data;

  struct gomp_thread local_thr;

  thr = &local_thr;
  marcel_setspecific (gomp_tls_key, thr);

  gomp_sem_init (&thr->release, 0);

  /* Extract what we need from data.  */
  local_fn = data->fn;
  local_data = data->fn_data;
  thr->ts = data->ts;

  thr->ts.team->ordered_release[thr->ts.team_id] = &thr->release;
  thr->tid = marcel_self ();
#ifdef HAVE_MARCEL_MAMI
  thr->memory_areas = tbx_slist_nil ();
  thr->has_attached_memory_pages = data->has_attached_memory_pages;
  thr->shake_on_team_creation = FGOMP_DONT_SHAKE;
#endif
  thr->private_num_threads = 0;
#ifdef LIBGOMP_USE_TASKS
  thr->untied_task_recursion_level = 0;
#endif

  char thread_name[64];
  marcel_snprintf (thread_name, 64, "%s%i", thr->ts.team->name, thr->ts.team_id);
  marcel_setname (MARCEL_SELF, thread_name);
  marcel_self ()->id = ma_atomic_add_return (1, &cpt);

  /* Whether preemption is enabled should have been specified as part of this
     thread's attributes.  */
  assert (!fgomp_disable_preemption == !marcel_thread_is_preemption_disabled ());

  if (data->nested)
    {
      gomp_init_task_region (data->depth);
      local_fn (local_data);
      gomp_destroy_task_region ();
      gomp_team_process_uninstantiated_tasks ();
      gomp_barrier_wait_start (&thr->ts.team->barrier, thr->ts.team_id, FGOMP_BARRIER_NO_WAIT_END);
#ifdef HAVE_MARCEL_MAMI
      gomp_memory_clean_thread (thr);
#endif
    }
  else
    {
      gomp_threads[thr->ts.team_id] = thr;
      gomp_barrier_wait (&gomp_threads_dock, thr->ts.team_id);

      do
	{
	  struct gomp_team *team;
	  int my_index = thr->ts.team_id;

          gomp_init_task_region (data->depth);
	  local_fn (local_data);
          gomp_destroy_task_region ();
          gomp_team_process_uninstantiated_tasks ();

	  /* Clear out the team and function data.  This is a debugging
	     signal that we're in fact back in the dock.  */

#ifdef HAVE_MARCEL_MAMI
	  gomp_memory_clean_thread (thr);
#endif
	  team = thr->ts.team;
	  thr->fn = NULL;
	  thr->data = NULL;
	  thr->ts.team = NULL;
	  thr->ts.work_share = NULL;
	  thr->ts.team_id = 0;
	  thr->ts.work_share_generation = 0;
	  thr->ts.static_trip = 0;

	  gomp_barrier_wait_start (&team->barrier, my_index, FGOMP_BARRIER_NO_WAIT_END);
	  gomp_barrier_wait (&gomp_threads_dock, my_index);
	  
	  local_fn = thr->fn;
	  local_data = thr->data;
	}
      while (local_fn);
    }

#ifdef HAVE_MARCEL_MAMI
  tbx_slist_free (thr->memory_areas);
#endif
  
  return NULL;
}


/* Initialize the team pointed to by OBJECT.  */
static error_t
initialize_team (void *hook, void *object)
{
  size_t nthreads;
  struct gomp_team *team = (struct gomp_team *) object;

  nthreads = (size_t) (uintptr_t) hook;

  gomp_mutex_init (&team->work_share_lock);

  team->work_shares = gomp_malloc (4 * sizeof (struct gomp_work_share *));
  team->generation_mask = 3;
  team->oldest_live_gen = 1;
  team->num_live_gen = 0;
  team->work_shares[0] = NULL;
  team->start_data = NULL;
  team->generation = gomp_thread ()->ts.team ? gomp_thread ()->ts.team->generation + 1 : 0;

  team->nthreads = nthreads;
  gomp_barrier_init (&team->barrier, nthreads, "team_barrier");

  gomp_sem_init (&team->master_release, 0);
  team->ordered_release[0] = &team->master_release;

  return 0;
}

/* Free the team pointed to by OBJECT.  */
static void
destroy_team (void *hook, void *object)
{
  struct gomp_team *team = (struct gomp_team *) object;

  gomp_free (team->work_shares);
  gomp_mutex_destroy (&team->work_share_lock);
  gomp_barrier_destroy (&team->barrier);
  gomp_sem_destroy (&team->master_release);
}

/* Try to reuse a team from the pool. */
static struct gomp_team *
new_team (unsigned nthreads, struct gomp_work_share *work_share, bool nested)
{
  size_t size;
  struct gomp_team *team = NULL;

  size = sizeof (*team) + nthreads * sizeof (team->ordered_release[0]);

  if (nthreads > 0 && nthreads < marcel_nbprocessors * 2)
    {
      error_t err;
      gomp_slab_space_t *space;

      space = &team_slab_spaces[nthreads];
      if (__builtin_expect (*space == NULL, 0))
	{
	  /* Create a new slab space for team with NTHREADS threads.  */
	  err = gomp_slab_create (size, __alignof__ (struct gomp_team),
				  NULL, NULL,
				  initialize_team, destroy_team,
				  (void *) (uintptr_t) nthreads,
				  space);
	  if (err)
	    *space = NULL;
	}

      if (__builtin_expect (*space != NULL, 1))
	{
	  err = gomp_slab_alloc (*space, (void *) &team);
	  if (err)
	    team = NULL;
	}
      else
	team = NULL;
    }

  if (__builtin_expect (team == NULL, 0))
    {
      team = gomp_malloc (size);
      if (team != NULL)
	initialize_team ((void *) (uintptr_t) nthreads, team);
    } 
  
  if (__builtin_expect (team != NULL, 1))
    {
      /* Initialize TEAM for WORK_SHARE.  */
      team->oldest_live_gen = work_share == NULL;
      team->num_live_gen = work_share != NULL;
      team->work_shares[0] = work_share;
      team->generation = gomp_thread()->ts.team ? gomp_thread()->ts.team->generation + 1 : 0;
      team->start_data = NULL;
      team->single_count = 0;

      if (nested)
	{
	  /* Make sure that all previous threads have left team->barrier. */
	  while (!gomp_barrier_is_available (&team->barrier))
	    marcel_yield ();
	}
    }

  return team;
}

/* Deallocate TEAM.  The dual of `new_team ()'.  */
static void
free_team (struct gomp_team *team)
{
  unsigned int nthreads;

  nthreads = team->nthreads;

  if (nthreads > 0 
      && nthreads < marcel_nbprocessors * 2 
      && team_slab_spaces[nthreads] != NULL)
    gomp_slab_dealloc (team_slab_spaces[nthreads], team);
  else
    {
      destroy_team (NULL, team);
      gomp_free (team);
    }
}


/* Launch a team.  */

void
gomp_team_start (void (*fn) (void *), void *data, unsigned nthreads,
		 struct gomp_work_share *work_share)
{
  struct gomp_thread_start_data *start_data;
  struct gomp_thread *thr, *nthr;
  struct gomp_team *team;
  bool nested;
  unsigned i, n, old_threads_used = 0;
  thr = gomp_thread ();
  nested = thr->ts.team != NULL;

  team = new_team (nthreads, work_share, nested);
  if (!thr->ts.team)
    marcel_sprintf (team->name, "fgomp-0:");
  else 
    marcel_sprintf (team->name, "%s%i:", thr->ts.team->name, thr->ts.team_id);
  
  marcel_attr_t gomp_thread_attr;
  marcel_attr_init(&gomp_thread_attr);
  marcel_attr_setdetachstate(&gomp_thread_attr, tbx_true);
  marcel_attr_setpreemptible (&gomp_thread_attr, !fgomp_disable_preemption);
  marcel_attr_setprio (&gomp_thread_attr, MA_DEF_PRIO);

  write_statistics (&fgomp_stats, FGOMP_STATS_PARALLEL);

  /* Keep thread seeds for nested parallel section */
  if (nested)
    {
#if 0   /* FIXME: Don't use thread seeds until the mechanism is fixed in Marcel. */
      /* Using seeds in cases where preemption (e.g., OpenMP 2.5, example A.18.1)
	 is needed requires that (i) seeds have a priority lower than
	 `MA_BATCH_PRIO' such as `MA_DEF_PRIO' so that seeds are scheduled
	 fairly, and (ii) seed runners inherit the priority of seeds.  The latter
	 appeared in Marcel as of SVN revision 18656.  */
      marcel_attr_setseed (&gomp_thread_attr, tbx_true);
#endif
    }

#ifdef LIBGOMP_USE_MARCEL_BUBBLES
  if (nthreads > 1 && nested)
    {
      marcel_bubble_t *holding_bubble = marcel_bubble_holding_task (thr->tid);
      ma_runqueue_t *top_rq = ma_get_parent_rq (ma_entity_bubble (holding_bubble));
      marcel_bubble_init (&team->bubble);
      marcel_bubble_scheduleonthreadholder (&team->bubble);
      marcel_bubble_setprio (&team->bubble, MA_DEF_PRIO);
      marcel_bubble_insertbubble (holding_bubble, &team->bubble);
      /* Make the main thread part of the borning bubble. */
      marcel_bubble_inserttask (&team->bubble, thr->tid);

      ma_topo_lock_all (top_rq->topolevel);
      /* Physically move the main thread inside the borning bubble. */
      ma_move_entity (ma_entity_task (marcel_self ()), &team->bubble.as_holder);
      ma_topo_unlock_all (top_rq->topolevel);

      marcel_attr_setnaturalbubble(&gomp_thread_attr, &team->bubble);
      /* Inherits from the master's `settled state'. Mostly used by
	 bubble schedulers. */
      team->bubble.as_entity.settled = ma_entity_task (marcel_self ())->settled;
#ifdef PROFILE
      fgomp_color_t *color = &fgomp_colors[thr->ts.team_id % FGOMP_NB_COLORS];
      ma_bubble_set_color (&team->bubble, color->r, color->g, color->b);
#endif
    }
  else
    marcel_attr_setnaturalbubble(&gomp_thread_attr, &marcel_root_bubble);
#endif

  gomp_init_task_region (gomp_tr()->depth+1);
  /* Always save the previous state, even if this isn't a nested team.
     In particular, we should save any work share state from an outer
     orphaned work share construct.  */
  team->prev_ts = thr->ts;

  thr->ts.team = team;
  thr->ts.work_share = work_share;
  thr->ts.team_id = 0;
  thr->ts.work_share_generation = 0;
#ifdef HAVE_SYNC_BUILTINS
  thr->ts.single_count = 0;
#endif
  thr->ts.static_trip = 0;

  if (nthreads == 1)
    return;

  i = 1;

  /* We only allow the reuse of idle threads for non-nested PARALLEL
     regions.  This appears to be implied by the semantics of
     threadprivate variables, but perhaps that's reading too much into
     things.  Certainly it does prevent any locking problems, since
     only the initial program thread will modify gomp_threads.  */
  if (!nested)
    {
      old_threads_used = gomp_threads_used;

      if (nthreads <= old_threads_used)
	n = nthreads;
      else if (old_threads_used == 0)
	{
	  n = 0;
	  gomp_barrier_init (&gomp_threads_dock, nthreads, "gomp_threads_dock");
	}
      else
	{
	  n = old_threads_used;

	  /* Increase the barrier threshold to make sure all new
	     threads arrive before the team is released.  */
	  gomp_barrier_reinit (&gomp_threads_dock, nthreads, "gomp_threads_dock", false);
	}

      /* Not true yet, but soon will be.  We're going to release all
	 threads from the dock, and those that aren't part of the
	 team will exit.  */
      gomp_threads_used = nthreads;

      /* Make sure the barriers are large enough.  */
      assert (team->barrier.count >= n);
      assert (gomp_threads_dock.count >= n);

      /* Release existing idle threads.  */
      for (; i < n; ++i)
	{
	  nthr = gomp_threads[i];
	  nthr->ts.team = team;
	  nthr->ts.work_share = work_share;
	  nthr->ts.team_id = i;
	  nthr->ts.work_share_generation = 0;
#ifdef HAVE_SYNC_BUILTINS
	  nthr->ts.single_count = 0;
#endif
	  nthr->ts.static_trip = 0;
	  nthr->fn = fn;
	  nthr->data = data;
	  team->ordered_release[i] = &nthr->release;

#ifdef HAVE_MARCEL_MAMI
	  nthr->has_attached_memory_pages = !tbx_slist_is_nil (thr->memory_areas);
	  gomp_memory_attach_to_thread (thr, &nthr->tid, i);
#endif
	}

      if (i == nthreads)
	goto do_release;

      /* If necessary, expand the size of the gomp_threads array.  It is
	 expected that changes in the number of threads is rare, thus we
	 make no effort to expand gomp_threads_size geometrically.  */
      if (nthreads >= gomp_threads_size)
	{
	  gomp_threads_size = nthreads + 1;
	  gomp_threads
	    = gomp_realloc (gomp_threads,
			    gomp_threads_size
			    * sizeof (struct gomp_thread_data *));
	}
    }

  team->start_data = gomp_malloc (sizeof (struct gomp_thread_start_data)
			    * (nthreads-i));

  assert (team->barrier.count >= nthreads);
  assert (nested || gomp_threads_dock.count >= nthreads);

  /* Launch new threads.  */
  for (start_data = team->start_data; i < nthreads; ++i, ++start_data)
    {
      marcel_t pt;
      int err;

      start_data->ts.team = team;
      start_data->ts.work_share = work_share;
      start_data->ts.team_id = i;
      start_data->ts.work_share_generation = 0;
      start_data->ts.static_trip = 0;
      start_data->fn = fn;
      start_data->fn_data = data;
      start_data->nested = nested;
#ifdef LIBGOMP_USE_TASKS
      start_data->depth = gomp_tr()->depth;
#endif
#ifdef HAVE_MARCEL_MAMI
      start_data->has_attached_memory_pages = !tbx_slist_is_nil (thr->memory_areas);
#endif

      err = marcel_create (&pt, &gomp_thread_attr,
			    gomp_thread_start, start_data);
#ifdef HAVE_MARCEL_MAMI
      gomp_memory_attach_to_thread (thr, &pt, i);
#endif

      if (err != 0)
	gomp_fatal ("Thread creation failed: %s", strerror (err));
    }

  /* We have just created a new team, update the running_teams
     statistics. */
  ma_atomic_inc (&fgomp_running_teams);
  
  marcel_bubble_submit ((nthreads > 1 && nested) ? &team->bubble : &marcel_root_bubble);
  
 do_release:

#ifdef HAVE_MARCEL_MAMI
    gomp_team_shake (team);
#endif

  /* Decrease the barrier threshold to match the number of threads
     that should arrive back at the end of this team.  The extra
     threads should be exiting.  Note that we arrange for this test
     to never be true for nested teams.  */
  if (nthreads < old_threads_used)
    gomp_barrier_reinit (&gomp_threads_dock, nthreads, "gomp_threads_dock", false);

  if (!nested)
    gomp_barrier_wait (&gomp_threads_dock, thr->ts.team_id);

#ifdef HAVE_MARCEL_MAMI

  /* Clean the main thread's memory areas. */
  gomp_memory_clean_thread (thr);

#endif
}


/* Terminate the current team.  This is only to be called by the master
   thread.  We assume that we must wait for the other threads.  */

void
gomp_team_end (void)
{
  struct gomp_thread *thr = gomp_thread ();
  struct gomp_team *team = thr->ts.team;

  gomp_destroy_task_region ();
  gomp_team_process_uninstantiated_tasks ();

  /* Make sure the team is being closed by the `main' thread. */
  assert (thr->ts.team_id == 0);
  assert (thr->tid == marcel_self ());

  gomp_barrier_wait (&team->barrier, thr->ts.team_id);

#ifdef LIBGOMP_USE_MARCEL_BUBBLES
  if (team->nthreads > 1 &&  team->prev_ts.team != NULL)
    {
      marcel_bubble_t *holding_bubble = marcel_bubble_holding_bubble (&team->bubble);
      ma_runqueue_t *prev_rq = ma_get_parent_rq (ma_entity_bubble (&team->bubble));
      ma_runqueue_t *top_rq = ma_get_parent_rq (ma_entity_bubble (holding_bubble));
      struct marcel_topo_level *top_level = marcel_topo_common_ancestor (prev_rq->topolevel, top_rq->topolevel);

      marcel_bubble_inserttask (holding_bubble, marcel_self ());

      if (top_rq != prev_rq)
	{
	  ma_topo_lock_all (top_level);
	  ma_move_entity (ma_entity_task (marcel_self ()), &prev_rq->as_holder);
	  ma_topo_unlock_all (top_level);
	}
      
      marcel_bubble_join (&team->bubble);
      marcel_bubble_destroy (&team->bubble);
      
      ma_atomic_dec (&fgomp_running_teams);
    }
#endif

  thr->ts = team->prev_ts;

  gomp_free (team->start_data);

  free_team (team);
}


#if (defined HAVE_PUKABI) && (!defined MARCEL_LIBPTHREAD)

#include <dlfcn.h>

/* Issue a warning if PukABI symbols don't prevail over libc symbols, i.e.,
   if PukABI was not preloaded.  */
static void
assert_pukabi_preloaded (void)
{
  void *self, *pukabi;
  bool determined = false;

  self = dlopen (NULL, RTLD_LAZY);
  pukabi = dlopen ("libPukABI.so", RTLD_LAZY);

  if (self != NULL && pukabi != NULL)
    {
      void *current_sym, *pukabi_sym;

      current_sym = dlsym (self, "malloc");
      pukabi_sym = dlsym (pukabi, "malloc");

      if (pukabi_sym != NULL && current_sym != NULL)
	{
	  determined = true;
	  if (pukabi_sym != current_sym)
	    {
	      fprintf (stderr, "[ForestGOMP] it appears that PukABI "
		       "was not preloaded\n");
	      fprintf (stderr, "[ForestGOMP] make sure to preload it "
		       "using the `LD_PRELOAD' environment variable\n");
	    }
	}
    }

  if (!determined)
    fprintf (stderr, "[ForestGOMP] unable to determine whether "
	     "PukABI was correctly preloaded\n");

  if (self != NULL)
    dlclose (self);

  if (pukabi != NULL)
    dlclose (pukabi);
}

#endif

/* Constructors for this file.  */
static void __attribute__((constructor))
team_constructor (void)
{
  struct gomp_thread *thr;

  if (!marcel_test_activity ())
    {
      int argc = 1;
      static char *argv[] = { "forestgomp", NULL };

      marcel_init (&argc, argv);
      marcel_ensure_abi_compatibility (MARCEL_HEADER_HASH);
    }

#if (defined HAVE_PUKABI) && (!defined MARCEL_LIBPTHREAD)
  /* Initialize PukABI.  */
  puk_abi_init ();

  /* Tell PukABI how to protect libc calls.  */
  puk_abi_set_spinlock_handlers ((void (*) (void)) marcel_extlib_protect,
				 (void (*) (void)) marcel_extlib_unprotect,
				 NULL);

  puk_abi_set_errno_handler (&marcel___errno_location);
  puk_abi_seterrno (0);

  assert_pukabi_preloaded ();
#endif

#ifdef HAVE_MARCEL_MAMI

  mami_init (&fgomp_memory_manager);

#ifdef HAVE_MARCEL_BUBBLE_CURRENT_SCHED

  marcel_bubble_sched_t *sched;

  sched = marcel_bubble_current_sched ();
  if (marcel_bubble_sched_class (sched) == &marcel_bubble_memory_sched_class)
    /* Since we are using a `memory' bubble scheduler, give it a handle to
       our memory manager.  */
    marcel_bubble_set_memory_manager ((marcel_bubble_memory_sched_t *) sched,
				      fgomp_memory_manager);

#else /* !HAVE_MARCEL_BUBBLE_CURRENT_SCHED */

  /* Use the old prototype as it existed prior to SVN revision 21167 of
     PM2/Marcel.  XXX: With PM2 revisions between 21167 and 21424 the
     following code doesn't compile.  */
  marcel_bubble_set_memory_manager (fgomp_memory_manager);

#endif /* !HAVE_MARCEL_BUBBLE_CURRENT_SCHED */

  /* fgomp_malloc () default behaviour. */
  int err = mami_membind (fgomp_memory_manager, MAMI_MEMBIND_POLICY_FIRST_TOUCH, 0);
  if (err < 0)
    abort ();

#endif /* HAVE_MARCEL_MAMI */

  ma_atomic_init (&cpt, 0);
  ma_atomic_init (&fgomp_running_teams, 0);

  static struct gomp_thread initial_thread_tls_data;

  marcel_key_create (&gomp_tls_key, NULL);
  marcel_setspecific (gomp_tls_key, &initial_thread_tls_data);

#ifdef LIBGOMP_USE_TASKS
  marcel_key_create (&gomp_tr_key, NULL);
#endif

  thr = &initial_thread_tls_data;
  thr->tid = marcel_self ();

  team_slab_spaces = gomp_malloc_cleared (sizeof (gomp_slab_space_t) * marcel_nbprocessors * 2);

  gomp_init_task_region (0);

#ifdef HAVE_MARCEL_MAMI
  thr->memory_areas = tbx_slist_nil ();
  thr->shake_on_team_creation = FGOMP_DONT_SHAKE;
#endif

  gomp_sem_init (&thr->release, 0);
  gomp_barrier_init (&gomp_threads_dock, 1, "gomp_threads_dock");

#ifdef LIBGOMP_USE_CONFIG_INFO
  marcel_printf("... ForestGOMP OpenMP runtime version %s\n", VERSION);
#ifdef HAVE_MARCEL_BUBBLE_CURRENT_SCHED
  marcel_printf("... - selected bubble scheduler: \t%s\n", marcel_bubble_current_sched()->klass->name);
#endif
#ifdef LIBGOMP_USE_TASKS
  marcel_printf("... - OpenMP 3.0 tasking support: \tenabled\n");
#else
  marcel_printf("... - OpenMP 3.0 tasking support: \tdisabled\n");
#endif
#ifdef HAVE_MARCEL_MAMI
  marcel_printf("... - MAMI memory management: \tenabled\n");
#else
  marcel_printf("... - MAMI memory management: \tdisabled\n");
#endif
#ifdef PROFILE
  marcel_printf("... - FxT profiling: \tenabled\n");
#else
  marcel_printf("... - FxT profiling: \tdisabled\n");
#endif
  marcel_printf("... ---\n");
#endif

  marcel_bubble_sched_begin ();
}

void __attribute__((destructor))
team_destructor (void)
{
  unsigned int level;

  print_statistics (&fgomp_stats);
  gomp_task_display_stats ();

  if (gomp_threads_used) {
    gomp_barrier_wait (&gomp_threads_dock, gomp_thread()->ts.team_id);
    gomp_barrier_destroy (&gomp_threads_dock);
  }

  /* Destroy the pool of ForestGOMP teams. */
  for (level = 0; level < marcel_nbprocessors * 2; level++)
    {
      if (team_slab_spaces[level] != NULL)
	{
	  error_t err;

	  err = gomp_slab_free (team_slab_spaces[level]);
	  if (err)
	    /* There may be live objects left in this slab space.  */
	    marcel_fprintf (stderr, "gomp_slab_free (%i): %m\n", level);
	}
    }

  gomp_free (team_slab_spaces);

#ifdef HAVE_MARCEL_MAMI
  gomp_memory_clean_thread (gomp_thread ());
  tbx_slist_free (gomp_thread ()->memory_areas);
  mami_exit (&fgomp_memory_manager);
#endif

  if (marcel_test_activity ())
    marcel_end ();
}

bool fgomp_lonesome_team()
{
  return (ma_atomic_read (&fgomp_running_teams) == 1) ? true : false;
}

void
fgomp_set_current_thread_load (unsigned int new_load)
{
  marcel_task_stats_set (long, marcel_self (), LOAD, new_load);
}

void
fgomp_set_num_threads (int num_threads)
{
  struct gomp_thread *thr = gomp_thread ();
  MA_BUG_ON (!thr);

  thr->private_num_threads = num_threads;
}

int
fgomp_get_num_threads ()
{
  struct gomp_thread *thr = gomp_thread ();
  MA_BUG_ON (!thr);

  return thr->private_num_threads ? thr->private_num_threads : gomp_nthreads_var;
}

int
fgomp_get_location (void)
{
  return marcel_current_vp ();
}

void
fgomp_say_hi (const char *msg)
{
  int me = gomp_thread ()->ts.team_id;

  marcel_printf ("[%s] FGOMP thread %i says hi from %s\n", msg, me, ma_entity_task (marcel_self ())->sched_holder->name);
}
