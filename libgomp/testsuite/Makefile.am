## Process this file with automake to produce Makefile.in.

AUTOMAKE_OPTIONS = foreign dejagnu

DEJATOOL = libgomp
RUNTEST = $(builddir)/libgomp-runtest


# Stand-alone tests.
TESTS =						\
  barriers/t-destroy				\
  barriers/t-non-power-of-2			\
  barriers/t-reinit				\
  barriers/t-reinit-shrink			\
  barriers/t-wait

TESTS_ENVIRONMENT = FORESTGOMP_DISABLE_PREEMPTION=false

if HAVE_PUKABI
TESTS_ENVIRONMENT += LD_PRELOAD="$(PM2_LIBDIR)/libPukABI.so"
endif HAVE_PUKABI


check_PROGRAMS = $(TESTS)

AM_CFLAGS = $(PM2_CFLAGS)
AM_CPPFLAGS = -I$(srcdir)/barriers						\
  -I$(top_srcdir)/config/marcel -I$(top_srcdir) -I$(top_builddir)
AM_LDFLAGS = $(PM2_LDFLAGS) -Wl,--rpath="$(PM2_LIBDIR)"

test_extra_sources =				\
  $(top_srcdir)/config/marcel/bar.c		\
  $(top_srcdir)/slab.c

barriers_t_wait_SOURCES = barriers/t-wait.c $(test_extra_sources)
barriers_t_reinit_SOURCES = barriers/t-reinit.c $(test_extra_sources)
barriers_t_reinit_shrink_SOURCES = barriers/t-reinit-shrink.c $(test_extra_sources)
barriers_t_destroy_SOURCES = barriers/t-destroy.c $(test_extra_sources)
barriers_t_non_power_of_2_SOURCES = barriers/t-non-power-of-2.c $(test_extra_sources)

noinst_HEADERS = include/i386-cpuid.h include/marcel-compat.h barriers/bar.h

DEJAGNU_LIBRARY =				\
  lib/gcc/obj-c++-dg.exp			\
  lib/gcc/treelang.exp				\
  lib/gcc/dejapatches.exp			\
  lib/gcc/c-torture.exp				\
  lib/gcc/g++-dg.exp				\
  lib/gcc/gcc-dg.exp				\
  lib/gcc/scanrtl.exp				\
  lib/gcc/profopt.exp				\
  lib/gcc/gnat.exp				\
  lib/gcc/prune.exp				\
  lib/gcc/copy-file.exp				\
  lib/gcc/target-libpath.exp			\
  lib/gcc/scanasm.exp				\
  lib/gcc/wrapper.exp				\
  lib/gcc/obj-c++.exp				\
  lib/gcc/target-supports-dg.exp		\
  lib/gcc/gfortran-dg.exp			\
  lib/gcc/mike-g++.exp				\
  lib/gcc/gcc-defs.exp				\
  lib/gcc/treelang-dg.exp			\
  lib/gcc/fortran-torture.exp			\
  lib/gcc/dg-pch.exp				\
  lib/gcc/objc-torture.exp			\
  lib/gcc/compat.exp				\
  lib/gcc/g++.exp				\
  lib/gcc/scanipa.exp				\
  lib/gcc/mike-gcc.exp				\
  lib/gcc/scantree.exp				\
  lib/gcc/objc-dg.exp				\
  lib/gcc/gcov.exp				\
  lib/gcc/gfortran.exp				\
  lib/gcc/gcc.exp				\
  lib/gcc/scandump.exp				\
  lib/gcc/gnat-dg.exp				\
  lib/gcc/c-compat.exp				\
  lib/gcc/target-supports.exp			\
  lib/gcc/objc.exp				\
  lib/gcc/file-format.exp			\
  lib/libgomp-dg.exp

C_TESTS =					\
  libgomp.c/omp-single-1.c			\
  libgomp.c/omp-loop02.c			\
  libgomp.c/copyin-3.c				\
  libgomp.c/omp_workshare4.c			\
  libgomp.c/nestedfn-2.c			\
  libgomp.c/appendix-a/a.26.1.c			\
  libgomp.c/appendix-a/a.33.3.c			\
  libgomp.c/appendix-a/a.18.1.c			\
  libgomp.c/appendix-a/a.5.1.c			\
  libgomp.c/appendix-a/a.21.1.c			\
  libgomp.c/appendix-a/a.15.1.c			\
  libgomp.c/appendix-a/a.4.1.c			\
  libgomp.c/appendix-a/a.39.1.c			\
  libgomp.c/appendix-a/a.16.1.c			\
  libgomp.c/appendix-a/a.2.1.c			\
  libgomp.c/appendix-a/a.19.1.c			\
  libgomp.c/appendix-a/a.36.1.c			\
  libgomp.c/appendix-a/a.40.1.c			\
  libgomp.c/appendix-a/a.29.1.c			\
  libgomp.c/appendix-a/a.3.1.c			\
  libgomp.c/omp_hello.c				\
  libgomp.c/atomic-2.c				\
  libgomp.c/pr30494.c				\
  libgomp.c/single-1.c				\
  libgomp.c/pr24455.c				\
  libgomp.c/barrier-1.c				\
  libgomp.c/nested-2.c				\
  libgomp.c/pr32362-3.c				\
  libgomp.c/pr26943-2.c				\
  libgomp.c/atomic-1.c				\
  libgomp.c/omp-parallel-if.c			\
  libgomp.c/loop-1.c				\
  libgomp.c/copyin-2.c				\
  libgomp.c/shared-2.c				\
  libgomp.c/omp_matvec.c			\
  libgomp.c/omp-single-3.c			\
  libgomp.c/omp_workshare3.c			\
  libgomp.c/omp-parallel-for.c			\
  libgomp.c/sections-1.c			\
  libgomp.c/omp_reduction.c			\
  libgomp.c/pr26943-4.c				\
  libgomp.c/critical-1.c			\
  libgomp.c/omp-loop03.c			\
  libgomp.c/pr32362-1.c				\
  libgomp.c/nested-1.c				\
  libgomp.c/critical-2.c			\
  libgomp.c/single-2.c				\
  libgomp.c/ordered-1.c				\
  libgomp.c/omp-nested-1.c			\
  libgomp.c/nestedfn-4.c			\
  libgomp.c/loop-3.c				\
  libgomp.c/pr29947-1.c				\
  libgomp.c/shared-3.c				\
  libgomp.c/pr26171.c				\
  libgomp.c/reduction-1.c			\
  libgomp.c/ordered-2.c				\
  libgomp.c/shared-1.c				\
  libgomp.c/omp-loop01.c			\
  libgomp.c/ordered-3.c				\
  libgomp.c/ordered-fgomp-1.c			\
  libgomp.c/pr32362-2.c				\
  libgomp.c/copyin-1.c				\
  libgomp.c/c.exp				\
  libgomp.c/omp_orphan.c			\
  libgomp.c/omp_workshare2.c			\
  libgomp.c/pr29947-2.c				\
  libgomp.c/vla-1.c				\
  libgomp.c/lib-1.c				\
  libgomp.c/pr26943-1.c				\
  libgomp.c/nestedfn-3.c			\
  libgomp.c/parallel-1.c			\
  libgomp.c/pr26943-3.c				\
  libgomp.c/loop-2.c				\
  libgomp.c/omp_workshare1.c			\
  libgomp.c/pr24455-1.c				\
  libgomp.c/omp-single-2.c			\
  libgomp.c/nestedfn-1.c			\
  libgomp.c/nestedfn-5.c			\
  libgomp.c/reduction-3.c			\
  libgomp.c/reduction-4.c			\
  libgomp.c/pr32468.c				\
  libgomp.c/atomic-10.c				\
  libgomp.c/reduction-2.c

CXX_TESTS =					\
  libgomp.c++/pr24455.C				\
  libgomp.c++/nested-1.C			\
  libgomp.c++/reduction-2.C			\
  libgomp.c++/single-3.C			\
  libgomp.c++/ctor-6.C				\
  libgomp.c++/reduction-3.C			\
  libgomp.c++/loop-6.C				\
  libgomp.c++/ctor-9.C				\
  libgomp.c++/ctor-4.C				\
  libgomp.c++/master-1.C			\
  libgomp.c++/pr24455-1.C			\
  libgomp.c++/loop-3.C				\
  libgomp.c++/ctor-2.C				\
  libgomp.c++/pr30703.C				\
  libgomp.c++/ctor-1.C				\
  libgomp.c++/ctor-3.C				\
  libgomp.c++/loop-5.C				\
  libgomp.c++/copyin-1.C			\
  libgomp.c++/copyin-2.C			\
  libgomp.c++/shared-1.C			\
  libgomp.c++/ctor-7.C				\
  libgomp.c++/ctor-8.C				\
  libgomp.c++/parallel-1.C			\
  libgomp.c++/loop-1.C				\
  libgomp.c++/reduction-1.C			\
  libgomp.c++/pr27337.C				\
  libgomp.c++/shared-2.C			\
  libgomp.c++/loop-2.C				\
  libgomp.c++/loop-7.C				\
  libgomp.c++/c++.exp				\
  libgomp.c++/ctor-5.C				\
  libgomp.c++/sections-1.C			\
  libgomp.c++/single-1.C			\
  libgomp.c++/loop-4.C				\
  libgomp.c++/pr26943.C				\
  libgomp.c++/single-2.C			\
  libgomp.c++/pr26691.C

FORTRAN_TESTS =					\
  libgomp.fortran/pr27916-2.f90			\
  libgomp.fortran/jacobi.f			\
  libgomp.fortran/pr27416-1.f90			\
  libgomp.fortran/reference1.f90		\
  libgomp.fortran/reduction2.f90		\
  libgomp.fortran/character2.f90		\
  libgomp.fortran/appendix-a/a.21.1.f90		\
  libgomp.fortran/appendix-a/a.31.5.f90		\
  libgomp.fortran/appendix-a/a.28.3.f90		\
  libgomp.fortran/appendix-a/a.3.1.f90		\
  libgomp.fortran/appendix-a/a.26.1.f90		\
  libgomp.fortran/appendix-a/a.39.1.f90		\
  libgomp.fortran/appendix-a/a.18.1.f90		\
  libgomp.fortran/appendix-a/a.31.4.f90		\
  libgomp.fortran/appendix-a/a.15.1.f90		\
  libgomp.fortran/appendix-a/a.28.5.f90		\
  libgomp.fortran/appendix-a/a.28.2.f90		\
  libgomp.fortran/appendix-a/a.5.1.f90		\
  libgomp.fortran/appendix-a/a.22.8.f90		\
  libgomp.fortran/appendix-a/a10.1.f90		\
  libgomp.fortran/appendix-a/a.38.1.f90		\
  libgomp.fortran/appendix-a/a.33.3.f90		\
  libgomp.fortran/appendix-a/a.16.1.f90		\
  libgomp.fortran/appendix-a/a.4.1.f90		\
  libgomp.fortran/appendix-a/a.28.4.f90		\
  libgomp.fortran/appendix-a/a.19.1.f90		\
  libgomp.fortran/appendix-a/a.2.1.f90		\
  libgomp.fortran/appendix-a/a.40.1.f90		\
  libgomp.fortran/appendix-a/a.28.1.f90		\
  libgomp.fortran/appendix-a/a.22.7.f90		\
  libgomp.fortran/sharing2.f90			\
  libgomp.fortran/retval1.f90			\
  libgomp.fortran/omp_parse2.f90		\
  libgomp.fortran/omp_reduction.f		\
  libgomp.fortran/threadprivate2.f90		\
  libgomp.fortran/reduction4.f90		\
  libgomp.fortran/nestedfn2.f90			\
  libgomp.fortran/condinc4.f90			\
  libgomp.fortran/vla5.f90			\
  libgomp.fortran/vla7.f90			\
  libgomp.fortran/do1.f90			\
  libgomp.fortran/omp_parse4.f90		\
  libgomp.fortran/character1.f90		\
  libgomp.fortran/reduction5.f90		\
  libgomp.fortran/threadprivate3.f90		\
  libgomp.fortran/omp_parse3.f90		\
  libgomp.fortran/omp_hello.f			\
  libgomp.fortran/pr27395-2.f90			\
  libgomp.fortran/pr27916-1.f90			\
  libgomp.fortran/condinc1.f			\
  libgomp.fortran/reference2.f90		\
  libgomp.fortran/pr28390.f			\
  libgomp.fortran/lib2.f			\
  libgomp.fortran/workshare1.f90		\
  libgomp.fortran/nestedfn3.f90			\
  libgomp.fortran/do2.f90			\
  libgomp.fortran/omp_cond2.f			\
  libgomp.fortran/retval2.f90			\
  libgomp.fortran/pr25219.f90			\
  libgomp.fortran/nestedfn1.f90			\
  libgomp.fortran/threadprivate1.f90		\
  libgomp.fortran/vla2.f90			\
  libgomp.fortran/crayptr1.f90			\
  libgomp.fortran/vla1.f90			\
  libgomp.fortran/reduction1.f90		\
  libgomp.fortran/reduction3.f90		\
  libgomp.fortran/pr27395-1.f90			\
  libgomp.fortran/omp_workshare1.f		\
  libgomp.fortran/omp_cond1.f			\
  libgomp.fortran/omp_atomic1.f90		\
  libgomp.fortran/pr29629.f90			\
  libgomp.fortran/omp_atomic2.f90		\
  libgomp.fortran/condinc3.f90			\
  libgomp.fortran/omp_orphan.f			\
  libgomp.fortran/lib3.f			\
  libgomp.fortran/reduction6.f90		\
  libgomp.fortran/vla4.f90			\
  libgomp.fortran/pr25162.f			\
  libgomp.fortran/condinc1.inc			\
  libgomp.fortran/condinc2.f			\
  libgomp.fortran/sharing1.f90			\
  libgomp.fortran/omp_workshare2.f		\
  libgomp.fortran/vla6.f90			\
  libgomp.fortran/fortran.exp			\
  libgomp.fortran/vla3.f90			\
  libgomp.fortran/omp_parse1.f90		\
  libgomp.fortran/lib1.f90

EXTRA_DIST = $(C_TESTS) $(CXX_TESTS) $(FORTRAN_TESTS) $(DEJAGNU_LIBRARY)

# GNU Fortran produces `.mod' files.
CLEANFILES = *.mod
