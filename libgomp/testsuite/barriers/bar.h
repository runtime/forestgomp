/* Copyright (C) 2008-2009 INRIA

   This file is part of the GNU OpenMP Library (libgomp).

   Libgomp is free software; you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 2.1 of the License, or
   (at your option) any later version.

   Libgomp is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License 
   along with libgomp; see the file COPYING.LIB.  If not, write to the
   Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.  */

#ifndef FGOMP_TEST_BAR_H
#define FGOMP_TEST_BAR_H

/* Include the real <bar.h> from ForestGOMP.  */
#include_next <bar.h>

/* XXX: Hack to provide `bar.o' with what it needs.  */
extern bool fgomp_lonesome_team (void)
  __attribute__ ((__weak__));

bool
fgomp_lonesome_team (void)
{
  return true;
}

#endif
