/* Copyright (C) 2008-2009 INRIA

   This file is part of the GNU OpenMP Library (libgomp).

   Libgomp is free software; you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 2.1 of the License, or
   (at your option) any later version.

   Libgomp is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License 
   along with libgomp; see the file COPYING.LIB.  If not, write to the
   Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.  */

/* Make sure barriers work well with a thread number that is not a power of
   two.  */

#include <marcel.h>

#include <stdlib.h>
#include <stdio.h>

#include <bar.h>


/* Number of times threads will wait on the barrier.  */
#define ITERATION_COUNT 100

/* A non-power-of-2 number of threads.  */
#define thread_count 77

struct thread_input
{
  gomp_barrier_t *barrier;
  unsigned int    index;
};

static void *
thread_entry_point (void *arg)
{
  unsigned i;
  const struct thread_input *input;

  input = (struct thread_input *) arg;

  for (i = 0; i < ITERATION_COUNT; i++)
    gomp_barrier_wait (input->barrier, input->index);

  return NULL;
}


int
main (int argc, char *argv[])
{
  gomp_barrier_t bar;
  unsigned i;

  marcel_init (&argc, argv);
  marcel_ensure_abi_compatibility (MARCEL_HEADER_HASH);

  gomp_barrier_init (&bar, thread_count, "bar");

  {
    marcel_t threads[thread_count];
    struct thread_input thread_input[thread_count];

    threads[0] = marcel_self ();
    for (i = 1; i < thread_count; i++)
      {
	thread_input[i].barrier = &bar;
	thread_input[i].index = i;
	marcel_create (&threads[i], NULL,
		       thread_entry_point, &thread_input[i]);
      }

    for (i = 0; i < ITERATION_COUNT; i++)
      gomp_barrier_wait (&bar, 0);

    for (i = 1; i < thread_count; i++)
      marcel_join (threads[i], NULL);
  }

  gomp_barrier_destroy (&bar);

  marcel_end ();

  return 0;
}
