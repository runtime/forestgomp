/* Copyright (C) 2009 INRIA

   This file is part of the GNU OpenMP Library (libgomp).

   Libgomp is free software; you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 2.1 of the License, or
   (at your option) any later version.

   Libgomp is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License 
   along with libgomp; see the file COPYING.LIB.  If not, write to the
   Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.  */

/* Make sure `gomp_barrier_reinit ()' is able to shrink a barrier, with the
   assumption that nobody is waiting on the barrier being shrunk (this
   assumption holds for internal uses of that function, in particular in
   `gomp_team_start ()' when shrinking GOMP_THREADS_DOCK.)  */

#undef NDEBUG
#include <marcel.h>

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#undef NDEBUG
#include <assert.h>

#include <bar.h>


/* Number of times threads will wait on the barrier.  */
#define ITERATION_COUNT 123

/* Number of threads.  */
#define THREAD_COUNT 32


struct thread_input
{
  gomp_barrier_t *barrier;
  unsigned int    index;
};

static void *
thread_entry_point (void *arg)
{
  unsigned i, total = 0;
  const struct thread_input *input;

  input = (struct thread_input *) arg;

  for (i = 0; i < ITERATION_COUNT; i++, total++)
    gomp_barrier_wait (input->barrier, input->index);

  return (void *) (uintptr_t) total;
}


int
main (int argc, char *argv[])
{
  marcel_bubble_sched_t *scheduler;
  gomp_barrier_t bar;
  unsigned i, j;
  int ret;

  marcel_init (&argc, argv);
  marcel_ensure_abi_compatibility (MARCEL_HEADER_HASH);

  scheduler = alloca (marcel_bubble_sched_instance_size (&marcel_bubble_cache_sched_class));
  ret = marcel_bubble_cache_sched_init ((struct marcel_bubble_cache_sched *) scheduler, marcel_topo_level (0,0), tbx_false);
  MA_BUG_ON (ret != 0);

  marcel_bubble_change_sched (scheduler);

  /* Initialize BAR for the total thread number.  */
  gomp_barrier_init (&bar, THREAD_COUNT, "barrier");

  for (j = 1; (THREAD_COUNT / j) > 1; j++)
    {
      unsigned thread_count = THREAD_COUNT / j;
      marcel_t threads[thread_count];
      struct thread_input thread_input[thread_count];

      threads[0] = marcel_self ();
      thread_input[0].barrier = &bar;
      thread_input[0].index = 0;

      for (i = 1; i < thread_count; i++)
	{
	  thread_input[i].barrier = &bar;
	  thread_input[i].index = i;
	  marcel_create (&threads[i], NULL,
			 thread_entry_point, &thread_input[i]);
	}

      /* Distribute the threads, otherwise this test can spend too much
	 time on large scale computers, due to high contention on the
	 "machine" level. */
      marcel_bubble_sched_begin ();
      
      for (i = 0; i < ITERATION_COUNT; i++)
	gomp_barrier_wait (&bar, 0);

      for (i = 1; i < thread_count; i++)
	{
	  uintptr_t total;
	  marcel_join (threads[i], (void **) &total);
	  assert (total == ITERATION_COUNT);
	}

      /* Shrink the barrier.  */
#if 0
      printf ("shrinking barrier from %u to %u threads\n",
	      thread_count, THREAD_COUNT / (j + 1));
#endif
      gomp_barrier_reinit (&bar, THREAD_COUNT / (j + 1), "smaller-barrier", true);

      /* Start again, with the shrunk barrier.  */
    }

  gomp_barrier_destroy (&bar);

  marcel_end ();

  return 0;
}
