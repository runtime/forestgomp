/* Copyright (C) 2008-2009 INRIA

   This file is part of the GNU OpenMP Library (libgomp).

   Libgomp is free software; you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 2.1 of the License, or
   (at your option) any later version.

   Libgomp is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License 
   along with libgomp; see the file COPYING.LIB.  If not, write to the
   Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.  */

/* Make sure barriers are not crossed earlier than expected.  */

#define MARCEL_INTERNAL_INCLUDE
#include <marcel.h>

#include <stdlib.h>
#include <stdio.h>

#include <bar.h>


/* Number of times threads will wait on the barrier.  */
#define ITERATION_COUNT 500

struct thread_input
{
  gomp_barrier_t *barrier;
  unsigned int    index;
  unsigned int    thread_count;
  unsigned int   *thread_passed;
};

#if defined( __i386__ ) || defined( __i486__ ) || defined( __i586__ ) || \
    defined( __i686__ ) || defined( __x86_64__ )
# define MBAR()  asm volatile ("mfence" ::: "memory")
#else
# define MBAR()  __sync_synchronize ()
#endif


static void *
thread_entry_point (void *arg)
{
  unsigned i;
  const struct thread_input *input;

  input = (struct thread_input *) arg;

  for (i = 0; i < ITERATION_COUNT; i++)
    {
      if (input->index == 0)
	/* At this point, no one should be beyond the barrier.  */
	assert (*input->thread_passed == 0);

      gomp_barrier_wait (input->barrier, input->index);

#ifdef __GNUC__
      __sync_fetch_and_add (input->thread_passed, 1);
#else
      *input->thread_passed++;
#endif

      gomp_barrier_wait (input->barrier, input->index);

      /* The progress bar.  */
      if (input->index == 0 && i % (ITERATION_COUNT / 70U) == 0)
	marcel_printf (".");

      if (input->index == 0)
	{
	  /* Every one should have successfully crossed the first barrier.  */
	  MBAR ();
	  assert (*input->thread_passed == input->thread_count);
	  *input->thread_passed = 0;
	  MBAR ();
	}
    }

  return NULL;
}


int
main (int argc, char *argv[])
{
  marcel_bubble_sched_t *scheduler;
  unsigned i, procs, thread_count, thread_passed = 0;
  int ret;

  setvbuf (stdout, NULL, _IONBF, 0);

  marcel_init (&argc, argv);
  marcel_ensure_abi_compatibility (MARCEL_HEADER_HASH);

  scheduler = alloca (marcel_bubble_sched_instance_size (&marcel_bubble_cache_sched_class));
  ret = marcel_bubble_cache_sched_init ((struct marcel_bubble_cache_sched *) scheduler, marcel_topo_level (0,0), tbx_false);
  MA_BUG_ON (ret != 0);
  
  marcel_bubble_change_sched (scheduler);

  procs = marcel_nbvps ();

  /* Try the following scenarios:

       1. "underload", i.e., fewer threads than cores;
       2. as many threads as cores;
       3. "overload", i.e., more threads than cores.

     The idea is to help identify which of these scenarios work.  */

  for (thread_count = procs > 1 ? procs / 2 : 1;
       thread_count < procs * 3;
       thread_count *= 2)
    {
      gomp_barrier_t bar;

      marcel_printf ("with %2u threads", thread_count);

      gomp_barrier_init (&bar, thread_count, "bar");

      {
	marcel_t threads[thread_count];
	struct thread_input thread_input[thread_count];

	threads[0] = marcel_self ();

	thread_input[0].barrier = &bar;
	thread_input[0].index = 0;
	thread_input[0].thread_count = thread_count;
	thread_input[0].thread_passed = &thread_passed;

	for (i = 1; i < thread_count; i++)
	  {
	    thread_input[i].barrier = &bar;
	    thread_input[i].index = i;
	    thread_input[i].thread_count = thread_count;
	    thread_input[i].thread_passed = &thread_passed;
	    marcel_create (&threads[i], NULL,
			   thread_entry_point, &thread_input[i]);
	  }
	
	/* Distribute the threads, otherwise this test can spend too much
	   time on large scale computers, due to high contention on the
	   "machine" level. */
	marcel_bubble_sched_begin ();

	thread_entry_point (&thread_input[0]);

	for (i = 1; i < thread_count; i++)
	  marcel_join (threads[i], NULL);
      }

      gomp_barrier_destroy (&bar);

      marcel_printf ("\n");
    }

  marcel_end ();

  return 0;
}
