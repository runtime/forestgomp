/* Copyright (C) 2008-2009 INRIA

   This file is part of the GNU OpenMP Library (libgomp).

   Libgomp is free software; you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 2.1 of the License, or
   (at your option) any later version.

   Libgomp is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with libgomp; see the file COPYING.LIB.  If not, write to the
   Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.  */

/* Source-level compatibility with Marcel.  Based on Marcel's
   <pthread.h>.  */

#ifndef GOMP_MARCEL_COMPAT_H
#define GOMP_MARCEL_COMPAT_H

#include <marcel.h>

#define printf(format, ...)          marcel_printf(format,##__VA_ARGS__)
#define fprintf(stream, format, ...) marcel_fprintf(stream, format,##__VA_ARGS__)
#define malloc(size)        tmalloc(size)
#define calloc(nmemb, size) tcalloc(nmemb, size)
#define realloc(ptr, size)  trealloc(ptr, size)
#define free(ptr)           tfree(ptr)

#ifdef __cplusplus

/* C++ replacements for the new/delete operator family.  */

#include <new>
#include <cstddef>

extern "C++" {

	inline void *
	operator new (size_t _size) throw(std::bad_alloc) {
		void *mem = marcel_malloc (_size, __FILE__, __LINE__);
		if (tbx_unlikely (mem == NULL))
			throw std::bad_alloc ();
		return mem;
	}

	inline void *
	operator new (size_t _size, const std::nothrow_t &_nt) throw() {
		return marcel_malloc (_size, __FILE__, __LINE__);
	}
	inline void *
	operator new[] (size_t _size) throw(std::bad_alloc) {
		void *mem = marcel_malloc (_size, __FILE__, __LINE__);
		if (tbx_unlikely (mem == NULL))
			throw std::bad_alloc ();
		return mem;
	}
	inline void *
	operator new[] (size_t _size, const std::nothrow_t &_nt) throw() {
		return marcel_malloc (_size, __FILE__, __LINE__);
	}

	inline void
	operator delete (void *mem) throw() {
		marcel_free (mem);
	}
	inline void
	operator delete (void *mem, const std::nothrow_t &_nt) throw() {
		marcel_free (mem);
	}
	inline void
	operator delete [] (void *mem) throw() {
		marcel_free (mem);
	}
	inline void
	operator delete [] (void *mem, const std::nothrow_t &_nt) throw() {
		marcel_free (mem);
	}
}

#endif /* __cplusplus */


/* A constructor that checks Marcel ABI compatibility at run-time.  This is
   needed since we include <marcel.h> here, which all test programs get to
   see.  */
static void __attribute__ ((__constructor__))
gomp_marcel_test_ctor (void)
{
  marcel_ensure_abi_compatibility (MARCEL_HEADER_HASH);
}

#endif

/*
	 Local Variables:
	 tab-width: 2
	 End:
 */
