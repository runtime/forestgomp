/* Loosely based on `libgomp.fortran/do2.f90'.

   Initially written to detect a bug introduced in SVN r161 of ForestGOMP,
   dated 2008-06-26.  */

#include <omp.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define ITERATIONS 100

int
main (int argc, char *argv[])
{
  bool k;
  int i, j, z;
  unsigned int singles = 0;

  k = false;

#pragma omp parallel num_threads (4) private (z)
  {
    for (z = 0; z < ITERATIONS; z++)
      {
#pragma omp single
	{
	  __sync_fetch_and_add (&singles, 1);
	  j = 28;
	}

#pragma omp for ordered schedule (static)
	for (i = 28; i <= 39; i += 2)
	  {
#pragma omp ordered
	    {
	      if (i != j)
		k = true;
	      j = j + 2;
	    }
	  }

#pragma omp single
	if (k)
	  abort ();
      }
  }

  if (singles != ITERATIONS)
    abort ();

  return 0;
}

